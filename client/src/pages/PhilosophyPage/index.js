import universal from 'utils/universal'

export const PhilosophyPageContainer = universal(() =>
  import(/* webpackChunkName: 'PhilosophyPageContainer' */ './PhilosophyPageContainer')
)
