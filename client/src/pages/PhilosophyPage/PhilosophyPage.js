import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'

class PhilosophyPage extends Component {
  render() {
    const { style } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Philosophy</title>
        </MetaTags>
        <h1 className="main-title">Philosophy</h1>
        <h3>Knowledge</h3>
        <ul>
          <li>
            There is no knowledge that is not power.
            <br /> <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            Knowledge will give you power, but character respect.
            <br /> <i>Bruce Lee</i>
          </li>
          <li>
            Learning is never cumulative, it is a movement of knowing which has
            no beginning and no end.
            <br />
            <i>Bruce Lee</i>
          </li>
          <li>
            Knowing is not enough; We must apply. Willing is not enough; We must
            do.
            <br />
            <i>Johann Wolfgang von Goethe</i>
          </li>
          <li>
            Anyone who stops learning is old, whether at twenty or eighty.
            Anyone who keeps learning stays young.
            <br />
            <i>Henry Ford</i>
          </li>
          <li>
            To earn more, you must learn more
            <br /> <i>Brian Tracy</i>
          </li>
          <li>
            Only the wisest and stupidest of men never change.
            <br /> <i>Confucius</i>
          </li>
          <li>
            Life is a succession of lessons, which must be lived to be
            understood.
            <br /> <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            If we encounter a man of rare intellect, we should ask him what
            books he reads.
            <br />
            <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            You can never be wise unless you love reading.
            <br />
            <i>Samuel Johnson</i>
          </li>
          <li>
            A thorough knowledge of the Bible is worth more than a college
            education.
            <br />
            <i>Theodore Roosevelt</i>
          </li>
          <li>
            Within the covers of the Bible are the answers for all the problems
            men face.
            <br />
            <i>Ronald Reagan</i>
          </li>
          <li>
            Blessed are those who find wisdom, those who gain understanding, for
            she is more profitable than silver and yields better returns than
            gold. She is more precious than rubies; nothing you desire can
            compare with her. Long life is in her right hand; in her left hand
            are riches and honor. Her ways are pleasant ways, and all her paths
            are peace. She is a tree of life to those who take hold of her;
            those who hold her fast will be blessed.
            <br />
            <i>Proverbs 3:13-18</i>
          </li>
          <li>
            For the word of God is alive and active. Sharper than any
            double-edged sword, it penetrates even to dividing soul and spirit,
            joints and marrow; it judges the thoughts and attitudes of the
            heart.
            <br />
            <i>Hebrews 4:12</i>
          </li>
          <li>
            Reading… a vacation for the mind…
            <br />
            <i>Dave Barry</i>
          </li>
          <li>
            I cannot remember the books I’ve read any more than the meals I have
            eaten; even so, they have made me.
            <br />
            <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            No man was ever wise by chance.
            <br />
            <i>Seneca</i>
          </li>
          <li>
            Those who read books will always rule over those who watch TV.
            <br />
            <i>Unknown</i>
          </li>
          <li>
            Science is organized knowledge, wisdom is organized life.
            <br />
            <i>Kant</i>
          </li>
          <li>
            Real knowledge is to know the extent of one’s ignorance.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            Be silly. Be honest. Be kind.
            <br /> <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            The man who asks a question is a fool for a minute, the man who does
            not ask is a fool for life.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            A wise man can learn more from a foolish question than a fool can
            learn from a wise answer.
            <br />
            <i>Bruce Lee</i>
          </li>
          <li>
            For with much wisdom comes much sorrow; the more knowledge, the more
            grief.
            <br />
            <i>Ecclesiastes 1:18</i>
          </li>
          <li>
            Education breeds confidence. Confidence breeds hope. Hope breeds
            peace.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            Educating the mind without educating the heart is no education at
            all.
            <br />
            <i>Aristotle</i>
          </li>
          <li>
            Beware of false knowledge; it is more dangerous than ignorance.
            <br />
            <i>George Bernard Shaw</i>
          </li>
          <li>
            There is nothing more frightful than ignorance in action.
            <br />
            <i>Johann Wolfgang von Goethe</i>
          </li>
        </ul>
        <h3>Thinking</h3>
        <ul>
          <li>
            Consider fully, act decisively.
            <br />
            <i>Jigoro Kano</i>
          </li>
          <li>
            As you think, so shall you become.
            <br /> <i>Bruce Lee</i>
          </li>
          <li>
            The possession of anything begins in the mind.
            <br />
            <i>Bruce Lee</i>
          </li>
          <li>
            Your beliefs become your thoughts, Your thoughts become your words,
            Your words become your actions, Your actions become your habits,
            Your habits become your values, Your values become your destiny.
            <br /> <i>Mahatma Gandhi</i>
          </li>
          <li>
            Once we accept our limits, we go beyond them.
            <br /> <i>Albert Einstein</i>
          </li>
          <li>
            Whether you think you can, or you think you can’t–you’re right.
            <br />
            <i>Henry Ford</i>
          </li>
          <li>
            The only person who can stop you from reaching your goals is you.
            <br />
            <i>Jackie Joyner-Kersee</i>
          </li>
          <li>
            Learn advidly. Question it repeatedly. Analyze it carefully. Then
            put what you have learned into practice intelligently.
            <br /> <i>Confucius</i>
          </li>
          <li>
            Thinking is the hardest work there is, which is probably the reason
            so few engage in it.
            <br />
            <i>Henry Ford</i>
          </li>
          <li>
            The act of taking the first step is what separates the winners from
            the losers.
            <br />
            <i>Brian Tracy</i>
          </li>
          <li>
            Few people think more than two or three times a year; I have made an
            international reputation for myself by thinking once or twice a
            week.
            <br />
            <i>George Bernard Shaw</i>
          </li>
          <li>
            A good programmer is someone who always looks both ways before
            crossing a one-way street.
            <br />
            <i>Doug Linder</i>
          </li>
          <li>
            Carefully observe oneself and one's situation, carefully observe
            others, and carefully observe one's environment.
            <br /> <i>Jigoro Kano</i>
          </li>
          <li>
            He who learns but does not think is lost! He who thinks but does not
            learn is in great danger.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            We need much less than we think we need.
            <br /> <i>Maya Angelou</i>
          </li>
          <li>
            The only real valuable thing is intuition.
            <br /> <i>Albert Einstein</i>
          </li>
          <li>
            Have the courage to follow your heart and intuition. They somehow
            know what you truly want to become.
            <br />
            <i>Steve Jobs</i>
          </li>
          <li>
            Don’t let the noise of others’ opinions drown out your own inner
            voice.
            <br /> <i>Steve Jobs</i>
          </li>
          <li>
            You can’t connect the dots looking forward; you can only connect
            them looking backwards. So you have to trust that the dots will
            somehow connect in your future.
            <br /> <i>Steve Jobs</i>
          </li>
          <li>
            Logic will get you from A to Z; imagination will get you everywhere.
            <br />
            <i>Albert Einstein</i>
          </li>
          <li>
            Imagination is more important than knowledge.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            The people who are crazy enough to think they can change the world
            are the ones who do.
            <br />
            <i>Steve Jobs</i>
          </li>
          <li>
            We cannot solve our problems with the same thinking we used when we
            created them.
            <br /> <i>Albert Einstein</i>
          </li>
          <li>
            No problem can be solved from the same level of consciousness that
            created it.
            <br /> <i>Albert Einstein</i>
          </li>
          <li>
            A clever person solves a problem. A wise person avoids it.
            <br />
            <i>Albert Einstein</i>
          </li>
        </ul>
        <h3>Faith</h3>
        <ul>
          <li>
            Then Jesus told him, “Because you have seen me, you have believed;
            blessed are those who have not seen and yet have believed.
            <br /> <i>John 20:29</i>
          </li>
          <li>
            Never be afraid to trust an unknown future to a known God
            <br />
            <i>Corrie ten Boom</i>
          </li>
          <li>
            None of us knows what might happen even the next minute, yet still
            we go forward. Because we trust. Because we have faith. Or, who
            knows, perhaps because we just don't see the mystery contained in
            the next second
            <br />
            <i>Paulo Coelho</i>
          </li>
          <li>
            Without faith, nothing is possible. With it, nothing is impossible.
            <br />
            <i>Mary McLeod Bethune</i>
          </li>
          <li>
            And whatever you ask in prayer, you will receive, if you have faith.
            <br />
            <i>Matthew 21:22</i>
          </li>
          <li>
            Don't let life bring you down. Remember that God gives you struggles
            to test your faith.
            <br />
            <i>Paulo Coelho</i>
          </li>
          <li>
            Shallow men believe in luck. Strong men believe in cause and effect.
            <br /> <i>Ralf Waldo Emerson</i>
          </li>
          <li>
            Believing in destiny means not believing in yourself.
            <br /> <i>Charles Maurice de Talleyrand-Périgord</i>
          </li>
          <li>
            If you believe in yourself, anything is possible.
            <br />
            <i>Miley Cyrus</i>
          </li>
          <li>
            Magic is believing in yourself, if you can do that, you can make
            anything happen.
            <br />
            <i>Johann Wolfgang von Goethe</i>
          </li>
          <li>
            What you think of yourself is much more important than what others
            think of you.
            <br />
            <i>Seneca</i>
          </li>
          <li>
            Respect yourself and others will respect you.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            Sometimes life is going to hit you in the head with a brick. Don’t
            lose faith.
            <br />
            <i>Steve Jobs</i>
          </li>
          <li>
            The Teacher searched to find just the right words, and what he wrote
            was upright and true. The words of the wise are like goads, their
            collected sayings like firmly embedded nails — given by one
            shepherd. Be warned, my son, of anything in addition to them. Of
            making many books there is no end, and much study wearies the body.
            Now all has been heard; here is the conclusion of the matter: Fear
            God and keep his commandments, for this is the duty of all mankind.
            For God will bring every deed into judgment, including every hidden
            thing, whether it is good or evil.
            <br />
            <i>Ecclesiastes 12:10-14</i>
          </li>
        </ul>
        <h3>Order</h3>
        <ul>
          <li>
            Order is Heaven's first law; and this confessed, some are, and must
            be, greater than the rest, more rich, more wise; but who infers from
            hence that such are happier, shocks all common sense. Condition,
            circumstance, is not the thing; bliss is the same in subject or in
            king
            <br /> <i>Alexander Pope</i>
          </li>
          <li>
            Order is a great person's need and their true well being.
            <br /> <i>Henri Frederic Amiel</i>
          </li>
          <li>
            Order and simplification are the first steps toward the mastery of a
            subject.
            <br /> <i>Thomas Mann</i>
          </li>
          <li>
            Good order is the foundation of all things.
            <br /> <i>Edmund Burker</i>
          </li>
          <li>
            Order marches with weighty and measured strides. Disorder is always
            in a hurry.
            <br /> <i>Napoleon Bonaparte</i>
          </li>
          <li>
            We must have order, allocating to each thing its proper place and
            giving to each thing is due according to its nature.
            <br /> <i>Ludwig Mies van der Rohe</i>
          </li>
          <li>
            We must have order, allocating to each thing its proper place and
            giving to each thing is due according to its nature.
            <br /> <i>Ludwig Mies van der Rohe</i>
          </li>
          <li>
            There should be a place for everything, and everything in its place.
            <br /> <i>Isabella Beeton</i>
          </li>
        </ul>
        <h3>Goals</h3>
        <ul>
          <li>
            Commit to the Lord whatever you do, and he will establish your
            plans.
            <br />
            <i>Proverbs 16:3</i>
          </li>
          <li>
            Once you make a decision, the universe conspires to make it happen.
            <br />
            <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            Beware of your wishes: They will probably come true
            <br />
            <i>Edward Abbey</i>
          </li>
          <li>
            If you don't have big dreams and goals, you will end up working for
            someone who has them.
            <br />
            <i>Unknown</i>
          </li>
          <li>
            If you don’t know where you are going, you’ll end up someplace else.
            <br />
            <i>Yogi Berra</i>
          </li>
          <li>
            If a man knows not to which port he sails, no wind is favorable.
            <br /> <i>Seneca</i>
          </li>
          <li>
            Destitutis ventis remos adhibe.
            <br />
            If the wind does not serve, take to the oars
            <br /> <i>Latin proverb</i>
          </li>
          <li>
            If there is effort, there is always accomplishment.
            <br /> <i>Jigoro Kano</i>
          </li>
          <li>
            If you want to be a writer, you must do two things above all: read a
            lot and write a lot.
            <br />
            <i>Stephen King</i>
          </li>
          <li>
            The journey with a 1000 miles begins with one step.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            The man who moves a mountain begins by carrying away small stones.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            Diligence is the mother of good luck.
            <br />
            <i>Benjamin Franklin</i>
          </li>
          <li>
            What is not started today is never finished tomorrow.
            <br />
            <i>Johann Wolfgang von Goethe</i>
          </li>
          <li>
            Hapiness is not pleasure – it is victory.
            <br />
            <i>Ziglar</i>
          </li>
          <li>
            Everything you want is on the other side of fear.
            <br />
            <i>Jack Canfield</i>
          </li>
          <li>
            He who has overcome his fears will truly be free.
            <br />
            <i>Aristotle</i>
          </li>
          <li>
            If you want something you've never had You must be willing to do
            something you've never done.
            <br /> <i>Thomas Jefferson</i>
          </li>
          <li>
            Empty your cup so that it may be filled; become devoid to gain
            totality.
            <br />
            <i> Bruce Lee</i>
          </li>
          <li>
            Seize the initiative in whatever you undertake.
            <br /> <i>Jigoro Kano</i>
          </li>
          <li>
            Nothing great was ever achieved without enthusiasm.
            <br />
            <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            Without ambition one starts nothing. Without work one finishes
            nothing. The prize will not be sent to you. You have to win it.
            <br />
            <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            The one who has the passion, but no opportunity, usually works for
            the one who has the opportunity, but no passion...
            <br />
            <i>Ashot Nadanian</i>
          </li>
          <li>
            Dignity does not consist in possessing honors, but in deserving
            them.
            <br />
            <i>Aristotle</i>
          </li>
          <li>
            Opportunities don’t happen, you create them.
            <br />
            <i>Chris Grosser</i>
          </li>
          <li>
            Everything around you that you call life was made up by people that
            were no smarter than you. And you can change it, you can influence
            it… Once you learn that, you’ll never be the same again.
            <br /> <i>Steve Jobs</i>
          </li>
          <li>
            Roads were made for journeys not destinations.
            <br /> <i>Confucius</i>
          </li>
          <li>
            A goal is not always meant to be reached, it often serves simply as
            something to aim at.
            <br /> <i>Bruce Lee</i>
          </li>
        </ul>
        <h3>Planning</h3>
        <ul>
          <li>
            To humans belong the plans of the heart, but from the Lord comes the
            proper answer of the tongue.
            <br />
            <i>Proverbs 16:1</i>
          </li>
          <li>
            Many are the plans in a person’s heart, but it is the Lord’s purpose
            that prevails.
            <br />
            <i>Proverbs 19:21</i>
          </li>
          <li>
            Success depends upon previous preparation, and without such
            preparation, there is sure to be failure.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            By failing to prepare, you are preparing to fail.
            <br />
            <i>Benjamin Franklin</i>
          </li>
          <li>
            A goal without a plan is just a wish.
            <br />
            <i>Antoine de Saint-Exupéry</i>
          </li>
          <li>
            Luck is what happens when preparation meets opportunity.
            <br />
            <i> Seneca</i>
          </li>
          <li>
            Our goals can only be reached through a vehicle of a plan, in which
            we must fervently believe, and upon which we must vigorously act.
            There is no other route to success.
            <br />
            <i>Picasso</i>
          </li>
          <li>
            Always have a plan, and believe in it. Nothing happens by accident.
            <br />
            <i>Chuck Knox</i>
          </li>
          <li>
            Never begin the day until it is finished on paper.
            <br />
            <i>Jim Rohn</i>
          </li>
          <li>
            Nothing is particularly hard if you divide it into small jobs.
            <br />
            <i> Henry Ford</i>
          </li>
          <li>
            When you feel that the goal is unattainable, do not change the goal
            - change your plan of action.
            <br />
            <i> Confucius</i>
          </li>
          <li>
            Time is money.
            <br />
            <i>Benjamin Franklin</i>
          </li>
          <li>
            There is a time for everything, and a season for every activity
            under the heavens: a time to be born and a time to die, a time to
            plant and a time to uproot, a time to kill and a time to heal, a
            time to tear down and a time to build, a time to weep and a time to
            laugh, a time to mourn and a time to dance, a time to scatter stones
            and a time to gather them, a time to embrace and a time to refrain
            from embracing, a time to search and a time to give up, a time to
            keep and a time to throw away, a time to tear and a time to mend, a
            time to be silent and a time to speak, a time to love and a time to
            hate, a time for war and a time for peace.
            <br />
            <i>Ecclesiastes 3:1-9</i>
          </li>
        </ul>
        <h3>Collaboration</h3>
        <ul>
          <li>
            A new command I give you: Love one another. As I have loved you, so
            you must love one another. By this everyone will know that you are
            my disciples, if you love one another.
            <br /> <i>John 13:34-35</i>
          </li>
          <li>
            It is easy to hate and it is difficult to love. This is how the
            whole scheme of things works. All good things are difficult to
            achieve, and bad things are very easy to get.
            <br /> <i>Confucius</i>
          </li>
          <li>
            What you do not want done to yourself, do not do to others.
            <br /> <i>Confucius</i>
          </li>
          <li>
            Alone we can do so little; together we can do so much.
            <br /> <i>Helen Keller</i>
          </li>
          <li>
            In this world, there are things you can only do alone, and things
            you can only do with somebody else. It’s important to combine the
            two in just the right amount.
            <br /> <i>Haruki Murakami</i>
          </li>
          <li>
            No one lives long enough to learn everything they need to learn
            starting from scratch. To be successful, we absolutely, positively
            have to find people who have already paid the price to learn the
            things that we need to learn to achieve our goals.
            <br /> <i>Brian Tracy</i>
          </li>
          <li>
            None of us, including me, ever do great things. But we can all do
            small things, with great love, and together we can do something
            wonderful.
            <br /> <i>Mother Teresa</i>
          </li>
          <li>
            Two are better than one, because they have a good return for their
            labor. If either of them falls down, one can help the other up. But
            pity anyone who falls and has no one to help them up. Also, if two
            lie down together, they will keep warm. But how can one keep warm
            alone? Though one may be overpowered, two can defend themselves. A
            cord of three strands is not quickly broken.
            <br /> <i>Ecclesiastes 4:9-12 </i>
          </li>
          <li>
            Coming together is a beginning, staying together is progress, and
            working together is success.
            <br /> <i>Henry Ford</i>
          </li>
          <li>
            One man can be a crucial ingredient on a team, but one man cannot
            make a team.
            <br /> <i>Kareem Abdul-Jabbar</i>
          </li>
          <li>
            The strength of the team is each individual member. The strength of
            each member is the team.
            <br /> <i>Phil Jackson</i>
          </li>
          <li>
            Great things in business are never done by one person; they're done
            by a team of people.
            <br /> <i>Steve Jobs</i>
          </li>
          <li>
            No one can whistle a symphony. It takes a whole orchestra to play
            it.
            <br /> <i>H.E. Luccock</i>
          </li>
          <li>
            Technology is nothing. What's important is that you have a faith in
            people, that they're basically good and smart, and if you give them
            tools, they'll do wonderful things with them.
            <br /> <i>Steve Jobs</i>
          </li>
          <li>
            People work better when they know what the goal is and why. It is
            important that people look forward to coming to work in the morning
            and enjoy working.
            <br /> <i>Elon Musk</i>
          </li>
          <li>
            In nature we never see anything isolated, but everything in
            connection with something else which is before it, beside it, under
            it and over it.
            <br /> <i>Johann Wolfgang von Goethe</i>
          </li>
          <li>
            In nature, everything is wisely thought out and arranged, everyone
            should do his work, and this wisdom is the supreme justice of life
            <br /> <i>Leonardo da Vinci</i>
          </li>
          <li>
            No one is irreplaceable, but there are unique.
            <br /> <i>Pablo Picasso</i>
          </li>
          <li>
            Ubi concordia, ibi victoria
            <br />
            Where there is unity there is always victory.
            <br /> <i>Publilius Syrus</i>
          </li>
          <li>
            I don’t pay good wages because I have a lot of money; I have a lot
            of money because I pay good wages.
            <br /> <i>Robert Bosch</i>
          </li>
          <li>
            He who has never learned to obey cannot be a good commander.
            <br /> <i>Aristotle</i>
          </li>
          <li>
            He who can, does. He who cannot, teaches.
            <br /> <i>George Bernard Shaw</i>
          </li>
          <li>
            It is difficult to bring people to goodness with lessons, but it is
            easy to do so by example.
            <br /> <i>Seneca</i>
          </li>
          <li>
            Every employer gets the employees he deserve.
            <br /> <i>Walter Gilby</i>
          </li>
          <li>
            If you are the smartest person in the room, then you are in the
            wrong room.
            <br /> <i>Confucius</i>
          </li>
          <li>
            Wrong connection will give you shock throughout your life but the
            right one will light up your life
            <br /> <i>Leonardo DiCaprio</i>
          </li>

          <li>
            Never contract friendship with a man that is not better than
            thyself.
            <br /> <i>Confucius</i>
          </li>
          <li>
            Keep away from people who try to belittle your ambitions.
            <br /> <i>Mark Twain</i>
          </li>
          <li>
            Do not give dogs what is sacred; do not throw your pearls to pigs.
            If you do, they may trample them under their feet, and turn and tear
            you to pie
            <br /> <i>Matthew 7:6</i>
          </li>
          <li>
            The prudent keep their knowledge to themselves, but a fool’s heart
            blurts out folly
            <br /> <i>Proverbs 12:23</i>
          </li>
          <li>
            Walk with the wise and become wise, for a companion of fools suffers
            harm
            <br /> <i>Proverbs 13:20</i>
          </li>
          <li>
            Stay away from a fool, for you will not find knowledge on their lips
            <br /> <i>Proverbs 14:7</i>
          </li>
          <li>
            When you see a good person, think of becoming like her/him. When you
            see someone not so good, reflect on your own weak points.
            <br /> <i>Confucius</i>
          </li>
          <li>
            Be tolerant with others and strict with yourself.
            <br /> <i>Marcus Aurelius</i>
          </li>
          <li>
            Be strict with yourself but least reproachful of others and
            complaint is kept afar.
            <br /> <i>Confucius</i>
          </li>
          <li>
            What the superior man seeks is in himself; what the small man seeks
            is in others.
            <br /> <i>Confucius</i>
          </li>
          <li>
            When a man points a finger at someone else, he should remember that
            four of his fingers are pointing at himself.
            <br /> <i>Louis Nizer</i>
          </li>
        </ul>
        <h3>Reputation</h3>
        <ul>
          <li>
            A false balance is an abomination to the Lord, but a just weight is
            his delight.
            <br /> <i>Proverbs 11:1</i>
          </li>
          <li>
            Trust starts with truth and ends with truth.
            <br /> <i>Santosh Kalwar</i>
          </li>
          <li>
            Trust is the glue of life. It’s the most essential ingredient in
            effective communication. It’s the foundational principle that holds
            all relationships.
            <br /> <i>Stephen R. Covey</i>
          </li>
          <li>
            Trust is like blood pressure. It’s silent, vital to good health, and
            if abused it can be deadly.
            <br /> <i>Frank Sonnenberg</i>
          </li>
          <li>
            Trust has to be earned, and should come only after the passage of
            time.
            <br /> <i>Arthur Ashe</i>
          </li>
          <li>
            You can't buy a good reputation; you must earn it.
            <br /> <i>Harvey Mackay</i>
          </li>
          <li>
            Reputation is only a candle, of wavering and uncertain flame, and
            easily blown out, but it is the light by which the world looks for
            and finds merit.
            <br /> <i>James Russell Lowell</i>
          </li>
          <li>
            Trust is like a vase, once it’s broken, though you can fix it, the
            vase will never be same again.
            <br /> <i>Walter Anderson</i>
          </li>
          <li>
            Glass, China, and Reputation, are easily crack’d, and never well
            mended.
            <br /> <i>Benjamin Franklin</i>
          </li>
          <li>
            No matter how honest they were to you, a single lie is enough to
            break your trust, forever.
            <br /> <i>Jasper Mishow</i>
          </li>
          <li>
            Trust takes years to build, seconds to break, and forever to repair.
            <br /> <i>Unknown</i>
          </li>
          <li>
            The worst thing that can happen to a man is to lose his money, the
            next worst his health, the next worst his reputation.
            <br /> <i>Samuel Butler</i>
          </li>
          <li>
            Trusting is hard. Knowing who to trust, even harder.
            <br /> <i>Maria V. Snyder</i>
          </li>
          <li>
            A good reputation is more valuable than money.
            <br /> <i>Publilius Syrus</i>
          </li>
          <li>
            I would rather lose money than trust.
            <br /> <i>Robert Bosch</i>
          </li>
        </ul>
        <h3>Responsibility</h3>
        <ul>
          <li>
            The price of greatness is responsibility.
            <br /> <i>Winston Churchill</i>
          </li>
          <li>
            Liberty means responsibility. That is why most men dread it.
            <br /> <i>George Bernard Shaw</i>
          </li>
          <li>
            Most people do not really want freedom, because freedom involves
            responsibility, and most people are frightened of responsibility.
            <br /> <i>Sigmund Freud</i>
          </li>
          <li>
            We are made wise not by the recollection of our past, but by the
            responsibility for our future.
            <br /> <i>George Bernard Shaw</i>
          </li>
          <li>
            Look at the word responsibility – “response-ability” – the ability
            to choose your response. Highly proactive people recognize that
            responsibility. They do not blame circumstances, conditions, or
            conditioning for their behavior. Their behavior is a product of
            their own conscious choice, based on values, rather than a product
            of their conditions, based on feeling.
            <br /> <i>Stephen Covey</i>
          </li>
        </ul>
        <h3>Quality</h3>
        <ul>
          <li>
            Do you see someone skilled in their work? They will serve before
            kings; they will not serve before officials of low rank.
            <br />
            <i>Proverbs 22:29</i>
          </li>
          <li>
            Quality is remembered long after the price is forgotten.
            <br />
            <i>Aldo Gucci</i>
          </li>
          <li>
            Quality is more important than quantity. One home run is much better
            than two doubles.
            <br />
            <i>Steve Jobs</i>
          </li>
          <li>
            The only way to go fast, is to go well.
            <br />
            <i>Robert C. Martin</i>
          </li>
          <li>
            Clean code always looks like it was written by someone who cares
            <br />
            <i>Michael Feathers</i>
          </li>
          <li>
            Of course, bad code can be cleaned up. But it’s very expensive
            <br />
            <i>Robert C. Martin</i>
          </li>
          <li>
            Functions should do one thing. They should do It well. They should
            do It only!
            <br />
            <i>Robert C. Martin</i>
          </li>
          <li>
            Code is read more than it is written
            <br />
            <i>Daniel Roy Greenfeld</i>
          </li>
          <li>
            You should name a variable using the same care with which you name a
            first-born child.
            <br />
            <i>Robert C. Martin</i>
          </li>
          <li>
            When you see commented-out code, delete it!
            <br />
            <i>Robert C. Martin</i>
          </li>
          <li>
            Every time you write a comment, you should grimace and feel the
            failure of your ability of expression.
            <br />
            <i>Robert C. Martin</i>
          </li>
          <li>
            Always code as if the guy who ends up maintaining your code will be
            a violent psychopath who knows where you live
            <br />
            <i>John Woods</i>
          </li>
          <li>
            Better do a small part of a job perfectly, than make the whole job
            ten times worse.
            <br />
            <i>Aristotle</i>
          </li>
          <li>
            Always do things right. This will gratify some people and astonish
            the rest.
            <br /> <i>Mark Twain</i>
          </li>
          <li>
            We are what we repeatedly do. Excellence, then, is not an act, but a
            habit.
            <br /> <i>Will Durant</i>
          </li>
          <li>
            Pleasure in the job puts perfection in the work.
            <br /> <i>Aristotle</i>
          </li>
          <li>
            Haste makes waste.
            <br /> <i>Benjamin Franklin</i>
          </li>
          <li>
            Excellence is never an accident. It is always the result of high
            intention, sincere effort, and intelligent execution; it represents
            the wise choice of many alternatives – choice, not chance,
            determines your destiny.
            <br /> <i>Aristotle</i>
          </li>
          <li>
            Employer and employee are equally dependent on the fate of their
            company.
            <br /> <i>Robert Bosch</i>
          </li>
        </ul>
        <h3>Simplification</h3>
        <ul>
          <li>
            The greatest ideas are the simplest.
            <br /> <i>William Golding</i>
          </li>
          <li>
            If you can’t explain it simply, you don’t understand it well enough.
            <br />
            <i>Albert Einstein</i>
          </li>
          <li>
            I’m as proud of many of the things we haven’t done as the things we
            have done. Innovation is saying no to a thousand things.
            <br /> <i>Steve Jobs</i>
          </li>
          <li>
            That’s been one of my mantras—focus and simplicity. Simple can be
            harder than complex; you have to work hard to get your thinking
            clean to make it simple.
            <br /> <i>Steve Jobs</i>
          </li>
          <li>
            Almost all quality improvement comes via simplification of design,
            manufacturing… layout, processes, and procedures.
            <br /> <i>Tom Peters</i>
          </li>
          <li>
            Simplicity is the keynote of all true elegance.
            <br /> <i>Coco Chanel</i>
          </li>
          <li>
            Simplicity is the soul of efficiency
            <br /> <i>Austin Freeman</i>
          </li>
          <li>
            There is no greatness where there is no simplicity, goodness and
            truth
            <br /> <i>Leo Tolstoy</i>
          </li>
          <li>
            Simplicity is the ultimate sophistication.
            <br /> <i>Leonardo da Vinci</i>
          </li>
        </ul>
        <h3>Difficulties</h3>
        <ul>
          <li>
            Never, never, never give up.
            <br /> <i>Winston Churchill</i>
          </li>
          <li>
            Most people give up just when they’re about to achieve success. They
            quit on the one yard line. They give up at the last minute of the
            game one foot from a winning touchdown.
            <br /> <i>Ross Perot</i>
          </li>
          <li>
            Giving up is the only sure way to fail.
            <br />
            <i>Gena Showalter</i>
          </li>
          <li>
            We lost because we told ourselves we lost.
            <br />
            <i>Leo Tolstoy</i>
          </li>
          <li>
            Do not pray for an easy life, pray for the strength to endure a
            difficult one.
            <br /> <i>Bruce Lee</i>
          </li>
          <li>
            The secret of life, though, is to fall seven times and to get up
            eight times.
            <br /> <i>Paulo Coelho</i>
          </li>
          <li>
            Our greatest glory is not in never falling, but in rising every time
            we fall.
            <br /> <i>Confucius</i>
          </li>
          <li>
            You may encounter many defeats, but you must not be defeated.
            <br /> <i>Maya Angelou</i>
          </li>
          <li>
            If you don't like what you receive change what you give
            <br /> <i>Castaneda</i>
          </li>
          <li>
            Your life only gets better when you get better
            <br /> <i>Brian Tracy</i>
          </li>
          <li>
            Life is a fight for territory and once you stop fighting for what
            you want, what you don’t want will automatically take over.
            <br /> <i>Les Brown</i>
          </li>
          <li>
            Take care to get what you like or you will be forced to like what
            you get.
            <br /> <i>George Bernard Shaw</i>
          </li>
          <li>
            Difficulties are opportunities to better things; they are
            stepping-stones to greater experience...When one door closes,
            another always opens; as a natural law it has to, to balance.
            <br /> <i>Bryan Adams</i>
          </li>
          <li>
            The gem cannot be polished without friction, nor man perfected
            without trials.
            <br /> <i>Chinese proverb</i>
          </li>
          <li>
            The crucible is for silver, and the furnace is for gold but the Lord
            tests hearts.
            <br /> <i>Proverbs 17:3</i>
          </li>
          <li>
            Fire is the test of gold; adversity, of strong men.
            <br /> <i>Seneca</i>
          </li>
          <li>
            I’ve learned that something constructive comes from every defeat.
            <br />
            <i>Tom Landry</i>
          </li>
          <li>
            Difficulties strengthen the mind, as labor does the body.
            <br /> <i>Seneca</i>
          </li>
          <li>
            There is no easy way from the earth to the stars.
            <br /> <i>Seneca</i>
          </li>
          <li>
            Everything is hard before it is easy.
            <br /> <i>Johann Wolfgang von Goethe</i>
          </li>
          <li>
            The difficulty comes from our lack of confidence.
            <br /> <i>Seneca</i>
          </li>
          <li>
            No gains without pains.
            <br /> <i>Benjamin Franklin</i>
          </li>
          <li>
            Learning is not child’s play; we cannot learn without pain.
            <br /> <i>Aristotle</i>
          </li>
          <li>
            Hard times create strong men, strong men create good times, good
            times create weak men, and weak men create hard times.
            <br /> <i>G. Michael Hopf</i>
          </li>
        </ul>
        <h3>Failures</h3>
        <ul>
          <li>
            Bad times have a scientific value. These are occasions a good
            learner would not miss.
            <br /> <i>Bruce Lee</i>
          </li>
          <li>
            Don’t fear failure. Not failure, but low aim, is the crime. In great
            attempts, it is glorious even to fail.
            <br />
            <i>Bruce Lee</i>
          </li>
          <li>
            If you make a mistake and do not correct it, this is called a
            mistake.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            Success does not consist in never making mistakes but in never
            making the same one a second time.
            <br />
            <i>George Bernard Shaw</i>
          </li>
          <li>
            Defeat is not defeat unless accepted as a reality in your own mind.
            <br />
            <i>Bruce Lee</i>
          </li>
          <li>
            To be wronged is nothing, unless you continue to remember it.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            Every artist was first an amateur.
            <br />
            <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            Unless you try to do something beyond what you have already
            mastered, you will never grow.
            <br />
            <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            Failure should be our teacher, not our undertaker. Failure is delay,
            not defeat. It is a temporary detour, not a dead end. Failure is
            something we can avoid only by saying nothing, doing nothing, and
            being nothing.
            <br />
            <i>Denis Waitley</i>
          </li>
          <li>
            Success is not final; failure is not fatal: It is the courage to
            continue that counts
            <br />
            <i>Winston S. Churchill</i>
          </li>
          <li>
            Success is normally found in pile of mistakes.
            <br />
            <i>Tim Fargo</i>
          </li>
          <li>
            Failure is only the opportunity more intelligently to begin again.
            <br />
            <i>Henry Ford</i>
          </li>
        </ul>
        <h3>Persistence</h3>
        <ul>
          <li>
            Choose a job you love, and you will never have to work a day in your
            life.
            <br /> <i>Confucius</i>
          </li>
          <li>
            People should pursue what they're passionate about. That will make
            them happier than pretty much anything else.
            <br /> <i>Elon Musk</i>
          </li>
          <li>
            Walk a single path, becoming neither cocky with victory nor broken
            with defeat, without forgetting caution when all is quiet or
            becoming frightened when danger threatens.
            <br />
            <i>Jigoro Kano</i>
          </li>

          <li>
            Patience, persistence, and perspiration make an unbeatable
            combination for success.
            <br /> <i>Napoleon Hill</i>
          </li>

          <li>
            Success isn’t always about greatness. It’s about consistency.
            Consistent hard work leads to success. Greatness will come.
            <br /> <i>Dwayne Johnson</i>
          </li>

          <li>
            Part of courage is simple consistency.
            <br /> <i>Peggy Noonan</i>
          </li>
          <li>
            Just do it
            <br /> <i>Nike</i>
          </li>
          <li>
            Just do what must be done. This may not be happiness, but it is
            greatness.
            <br /> <i>George Bernard Shaw</i>
          </li>

          <li>
            Enjoy when you can, and endure when you must.
            <br /> <i>Johann Wolfgang von Goethe</i>
          </li>
          <li>
            To think is easy. To act is hard. But the hardest thing in the world
            is to act in accordance with your thinking.
            <br /> <i>Johann Wolfgang von Goethe</i>
          </li>

          <li>
            If you persevere long enough, if you do the right things long
            enough, the right things will happen.
            <br /> <i>Manon Rheaume</i>
          </li>
          <li>
            Persistence can change failure into extraordinary achievement.
            <br /> <i>Marv Levy</i>
          </li>
          <li>
            Champions keep playing until they get it right.
            <br /> <i>Billie Jean King</i>
          </li>
          <li>
            Drop by drop is the water pot filled.
            <br />
            <i>Buddha</i>
          </li>
          <li>
            Success is the sum of small efforts repeated day in and day out.
            <br />
            <i>Robert Collier</i>
          </li>
          <li>
            There is no elevator to success, you have to take the stairs.
            <br />
            Zig Ziglar
          </li>
          <li>
            It does not matter how slowly you go so long as you do not stop.
            <br /> <i>Confucius</i>
          </li>
        </ul>
        <h3>Habits</h3>
        <ul>
          <li>
            Motivation is what gets you started. Habit is what keeps you going.
            <br />
            <i>Jim Rohn</i>
          </li>
          <li>
            You’ll never change your life until you change something you do
            daily. The secret of your success is found in your daily routine.
            <br />
            <i>John C. Maxwell</i>
          </li>
          <li>
            All people are the same; only their habits differ.
            <br />
            <i>Confucius</i>
          </li>
          <li>
            Good habits formed at youth make all the difference.
            <br />
            <i>Aristotle</i>
          </li>
          <li>
            Early to bed and early to rise makes a man healthy, wealthy and
            wise.
            <br />
            <i>Benjamin Franklin</i>
          </li>
          <li>
            It is easier to prevent bad habits than to break them.
            <br />
            <i>Benjamin Franklin</i>
          </li>
          <li>
            Who is strong? He that can conquer his bad habits.
            <br />
            <i>Benjamin Franklin</i>
          </li>
        </ul>
        <h3>Health</h3>
        <ul>
          <li>
            The first wealth is health.
            <br /> <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            Life is like riding a bicycle. To keep your balance, you must keep
            moving.
            <br /> <i>Albert Einstein</i>
          </li>
          <li>
            Cibi, potus, somni, venus omnia moderāta sint. <br />
            Food, drink, sleep, love should be moderate
            <br /> <i>Hippocrates</i>
          </li>
          <li>
            To keep the body in good health is a duty, otherwise we shall not be
            able to keep the mind strong and clear.
            <br /> <i>Buddha</i>
          </li>
          <li>
            He who has health has hope; and he who has hope has everything.
            <br /> <i>Arabian proverb</i>
          </li>
          <li>
            A fit body, a calm mind, a house full of love. These things cannot
            be bought – they must be earned.
            <br /> <i>Naval Ravikant</i>
          </li>
          <li>
            You can have all the money in the world, the best job, the best
            family and friends, make beautiful trips, but, if you don’t have
            your health, nothing else matters.
            <br /> <i>Naval Ravikant</i>
          </li>
        </ul>
        <h3>Word</h3>
        <ul>
          <li>
            We are the masters of the unsaid words, but slaves of those we let
            slip out.
            <br /> <i>Winston Churchill</i>
          </li>
          <li>
            Gracious words are a honeycomb, sweet to the soul and healing to the
            bones.
            <br /> <i>Proverbs 16:24</i>
          </li>
          <li>
            Silence is a true friend who never betrays.
            <br /> <i>Confucius</i>
          </li>
          <li>
            Well done is better than well said.
            <br /> <i>Benjamin Franklin</i>
          </li>
          <li>
            The words of the reckless pierce like sword, but the tongue of the
            wise brings healing.
            <br /> <i>Proverbs 12:18</i>
          </li>
          <li>
            The one who has knowledge uses words with restraint, and whoever has
            understanding is even-tempered. Even fools are thought wise if they
            keep silent, and discerning if they hold their tongues.
            <br /> <i>Proverbs 17:27-28</i>
          </li>
          <li>
            Wise men speak because they have something to say; Fools because
            they have to say something.
            <br /> <i>Plato</i>
          </li>
          <li>
            Those who guard their lips preserve their lives, but those who speak
            rashly will come to ruin
            <br /> <i>Proverbs 13:3</i>
          </li>
          <li>
            Keep your tongue from evil and your lips from telling lies and
            swearing. Turn from evil and do good; Seek peace and pursue it.
            <br /> <i>Psalms 34:13-14</i>
          </li>
          <li>
            Before you speak, learn/think, and before you fall ill, take care of
            your health
            <br /> <i>Sirach 34:13-14</i>
          </li>
          <li>
            A superior man is modest in his speech but exceeds in his actions.
            <br /> <i>Confucius</i>
          </li>
          <li>
            The superior man acts before he speaks, and afterwards speaks
            according to his action.
            <br /> <i>Confucius</i>
          </li>
          <li>
            In business, you don't get what you deserve, you get what you
            negotiate.
            <br /> <i>Chester L. Karrass</i>
          </li>
          <li>
            Like apples of gold in settings of silver is a ruling rightly given.
            <br /> <i>Proverbs 25:11</i>
          </li>
          <li>
            Anxiety weighs down the heart, but a kind word cheers it up
            <br /> <i>Proverbs 12:25</i>
          </li>
        </ul>
        <h3>Calmness</h3>
        <ul>
          <li>
            Calmness is the lake of mind, thankfulness is the lake of heart.
            <br /> <i>Anonymous</i>
          </li>
          <li>
            Respect your calmness because it is the loudest voice of your
            strength.
            <br />
            <i>Anonymous</i>
          </li>
          <li>
            Ships don’t sink because of the water around them; ships sink
            because of the water that gets in them. Don’t let what’s happening
            around you get inside you and weigh you down.
            <br /> <i>Anonymous</i>
          </li>
          <li>
            Your strength is in your calmness, in the clarity of your mind.
            Strength comes from putting the negative aside without reacting. Win
            in calmness, in consciousness, in balance.
            <br /> <i>Yogi Amrit Desai</i>
          </li>
          <li>
            We acquire the strength we have overcome.
            <br />
            <i>Ralph Waldo Emerson</i>
          </li>
          <li>
            The more tranquil a man becomes, the greater his success, his
            influence, his power for good. Calmness of mind is one of the
            beautiful jewels of wisdom. It is the result of long and patient
            effort in self-control. Its presence is an indication of ripened
            experience, and of a more than ordinary knowledge of the laws and
            operations of thought.
            <br /> <i>James Allen</i>
          </li>
          <li>
            You must be shapeless, formless, like water. When you pour water in
            a cup, it becomes the cup. When you pour water in a bottle, it
            becomes the bottle. When you pour water in a teapot, it becomes the
            teapot. Water can drip and it can crash. Become like water my
            friend.
            <br /> <i>Bruce Lee</i>
          </li>
          <li>
            Notice that the stiffest tree is most easily cracked, while the
            bamboo or willow survives by bending with the wind.
            <br /> <i>Bruce Lee</i>
          </li>
          <li>
            You can’t control what goes on outside, but you can control what
            goes on inside.
            <br /> <i>Unknown</i>
          </li>
          <li>
            When anger rises, think of the consequences.
            <br /> <i>Confucius</i>
          </li>
          <li>
            Hate, it has caused a lot of problems in the world, but has not
            solved one yet.
            <br /> <i>Maya Angelou</i>
          </li>
          <li>
            A quick temper will make a fool of you soon enough.
            <br /> <i>Bruce Lee</i>
          </li>
          <li>
            Whatever is begun in anger, ends in shame.
            <br /> <i>Benjamin Franklin</i>
          </li>
          <li>
            Whoever is slow to anger is better than the mighty, and he who rules
            his spirit than he who takes a city.
            <br /> <i>Proverbs 16:32</i>
          </li>
        </ul>
        <h3>Patience</h3>
        <ul>
          <li>
            The basis of any wisdom is patience.
            <br /> <i>Plato</i>
          </li>
          <li>
            I have just three things to teach: simplicity, patience, compassion.
            These three are your greatest treasures.
            <br />
            <i>Lao Tzu</i>
          </li>
          <li>
            Patience is a virtue, and I'm learning patience. It's a tough
            lesson.
            <br />
            <i>Elon Musk</i>
          </li>
          <li>
            To lose patience is to lose the battle.
            <br /> <i>Mahatma Gandhi</i>
          </li>
          <li>
            How many a man has thrown up his hands at a time when a little more
            effort, a little more patience would have achieved success.
            <br /> <i>Elbert Hubbard</i>
          </li>
          <li>
            He that can have patience can have what he will.
            <br /> <i>Benjamin Franklin</i>
          </li>
          <li>
            Patience is bitter, but its fruit is sweet.
            <br /> <i>Aristotle</i>
          </li>
          <li>
            One minute of patience, ten years of peace.
            <br /> <i>Greek Proverb</i>
          </li>
        </ul>
        <h3>Gratitude</h3>
        <ul>
          <li>
            Start each day with a positive thought and a grateful heart.
            <br /> <i>Roy T. Bennett, The Light in the Heart</i>
          </li>
          <li>
            Those who have the ability to be grateful are the ones who have the
            ability to achieve greatness
            <br /> <i>Steve Maraboli, Life, the Truth, and Being Free</i>
          </li>
          <li>
            While eating a fruit, remember the one who planted the tree
            <br /> <i>Proverb</i>
          </li>
          <li>
            At times, our own light goes out and is rekindled by a spark from
            another person. Each of us has cause to think with deep gratitude of
            those who have lighted the flame within us.
            <br /> <i>Albert Schweitzer</i>
          </li>
          <li>
            Always remember people who have helped you along the way, and don’t
            forget to lift someone up
            <br /> <i>Roy T. Bennett, The Light in the Heart</i>
          </li>
          <li>
            No one who achieves success does so without the help of others. The
            wise and confident acknowledge this help with gratitude.
            <br /> <i>Alfred North Whitehead</i>
          </li>
          <li>
            The deepest craving of human nature is the need to be appreciated.
            <br /> <i>William James</i>
          </li>
          <li>
            There is a calmness to a life lived in gratitude, a quiet joy
            <br /> <i>Ralph H. Blum</i>
          </li>
          <li>
            Be thankful for what you have; you’ll end up having more. If you
            concentrate on what you don’t have, you will never, ever have enough
            <br /> <i>Oprah Winfrey</i>
          </li>
        </ul>
        <h3>Kindness</h3>
        <ul>
          <li>
            Attitude is a choice. Happiness is a choice. Optimism is a choice.
            Kindness is a choice. Giving is a choice. Respect is a choice.
            Whatever choice you make makes you. Choose wisely...
            <br /> <i>Roy T. Bennett, The Light in the Heart</i>
          </li>
          <li>
            Be mindful. Be grateful. Be positive. Be true. Be kind
            <br /> <i>Roy T. Bennett, The Light in the Heart</i>
          </li>
          <li>
            Kindness is a language which the deaf can hear and the blind can see
            <br /> <i>Mark Twain</i>
          </li>
          <li>
            Help others without any reason and give without the expectation of
            receiving anything in return
            <br /> <i>Roy T. Bennett, The Light in the Heart</i>
          </li>
          <li>
            Give to everyone who asks you, and if anyone takes what belongs to
            you, do not demand it back.
            <br /> <i>Luke 6:30</i>
          </li>
          <li>
            Give, and it will be given to you. A good measure, pressed down,
            shaken together and running over, will be poured into your lap. For
            with the measure you use, it will be measured to you.
            <br /> <i>Luke 6:38</i>
          </li>
          <li>
            A generous person will prosper; whoever refreshes others will be
            refreshed
            <br /> <i>Proverbs 11:25</i>
          </li>
          s
          <li>
            Forget injuries, never forget kindnesses.
            <br /> <i>Confucius</i>
          </li>
          <li>
            I know that there is nothing better for people than to be happy and
            to do good while they live
            <br /> <i>Ecclesiastes 3:12</i>
          </li>
        </ul>
        <h3>Forgiveness</h3>
        <ul>
          <li>
            The simple truth is, we all make mistakes, and we all need
            forgiveness.
            <br /> <i>Desmond Tutu</i>
          </li>
          <li>
            The highest result of education is tolerance.
            <br /> <i>Helen Keller</i>
          </li>
          <li>
            It is for God to punish wicked people; we should learn to forgive.
            <br /> <i>Emily Brontë</i>
          </li>
          <li>
            Happiness is a choice. Forgiveness is a choice. Acceptance, anger,
            desire, love are all choices. Your time. Your choice.
            <br /> <i>Scott Shaw</i>
          </li>
          <li>
            There is no love without forgiveness, and there is no forgiveness
            without love.
            <br /> <i>Bryant H. McGill</i>
          </li>
          <li>
            Let us forgive each other – only then will we live in peace.
            <br /> <i>Leo Tolstoy</i>
          </li>
          <li>
            Before we can forgive one another, we have to understand one
            another.
            <br /> <i>Emma Goldman</i>
          </li>
          <li>
            For if you forgive others their trespasses, your heavenly Father
            will also forgive you.
            <br /> <i>Matthew 6:14</i>
          </li>
        </ul>
        <h3>Love</h3>
        <ul>
          <li>
            Love is patient, love is kind. It does not envy, it does not boast,
            it is not proud. It does not dishonor others, it is not
            self-seeking, it is not easily angered, it keeps no record of
            wrongs. Love does not delight in evil but rejoices with the truth.
            It always protects, always trusts, always hopes, always perseveres.
            Love never fails. But where there are prophecies, they will cease;
            where there are tongues, they will be stilled; where there is
            knowledge, it will pass away.
            <br /> <i>Corinthians 13:4-8</i>
          </li>
          <li>
            And so we know and rely on the love God has for us. God is love.
            Whoever lives in love lives in God, and God in them.
            <br /> <i>John 4:16</i>
          </li>
          <li>
            Love is like the wind, you can’t see it but you can feel it.
            <br /> <i>Nicholas Sparks</i>
          </li>
          <li>
            You must realize that people have the right to think differently
            than you think and not do what you expect them to do. They probably
            love you, but their love may not be the way you want it to be.
            <br /> <i>George Bernard Shaw</i>
          </li>
          <li>
            I do my thing and you do your thing. I am not in this world to live
            up to your expectations, And you are not in this world to live up to
            mine. You are you, and I am I, and if by chance we find each other,
            it's beautiful. If not, it can't be helped.
            <br /> <i>Frederick Salomon Perls</i>
          </li>
          <li>
            I’ve learned that people will forget what you said, people will
            forget what you did, but people will never forget how you made them
            feel.
            <br /> <i>Maya Angelou</i>
          </li>
          <li>
            Everything you love will probably be lost, but in the end, love will
            return in another way
            <br /> <i>Franz Kafka</i>
          </li>
          <li>
            There is no time for cut-and-dried monotony. There is time for work.
            And time for love. That leaves no other time.
            <br />
            <i>Coco Chanel</i>
          </li>
          <li>
            I would teach peace rather than war. I would inculcate love rather
            than hate.
            <br />
            <i>Albert Einstein</i>
          </li>
          <li>
            And now these three remain: faith, hope and love. But the greatest
            of these is love
            <br />
            <i>Corinthians 13:13</i>
          </li>
          <li>
            All the world’s a stage, And all the men and women merely players;
            They have their exits and their entrances; And one man in his time
            plays many parts, His acts being seven ages...
            <br />
            <i>William Shakespeare</i>
          </li>
          <li>
            Everything passes, only love remains.
            <br /> <i>Gregory Skovoroda</i>
          </li>
        </ul>
      </div>
    )
  }
}

export default PhilosophyPage
