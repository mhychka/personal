import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'

class AboutPage extends Component {
  render() {
    const { style } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | About Me</title>
        </MetaTags>
        <h1 className="main-title">About Me</h1>

        <p>
          Hey! My name is Max and I am a full stack web engineer, based in
          Kyiv/Ukraine. I have been developing web applications for over 14
          years and I am good in developing of an application`s logic (frontend
          or backend), microservices or single page applications. I handle API
          requests to backend, payment systems integration, social networks
          integration, google services, application`s performance, setting of
          databases, writing database queries. I work with such programming
          technologies as JavaScript, NodeJs, TypeScript, GraphQL, SQL and
          manage to develop a project\microservice from the very beginning or
          support any existing one.
        </p>

        <p>
          I like to make ideas workable, studying and trying new technologies. I
          prefer to work with people in a team, who are interested in a project
          and try to make it a good one, share ideas for innovation, and
          accomplish goals and objectives.
        </p>

        <p>
          Profoundly skilled in developing, testing software, and having the
          ability to integrate web services analysis, as well as update in-house
          applications for companies, I am passionate to improve the part, I am
          responsible for. To work on issues, requests, and new features to make
          the project better for users. These processes make my job nice and
          enjoyable.
        </p>

        <p>
          I always worry about the quality of the project and prefer to take one
          long-term project in a company at the same time instead of freelance.
          This strategy always leads me to create really good and quality
          products.
        </p>
      </div>
    )
  }
}

export default AboutPage
