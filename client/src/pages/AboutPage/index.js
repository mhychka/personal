import universal from 'utils/universal'

export const AboutPageContainer = universal(() =>
  import(/* webpackChunkName: 'AboutPageContainer' */ './AboutPageContainer')
)
