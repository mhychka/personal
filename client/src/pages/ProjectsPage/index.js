import universal from 'utils/universal'

export const ProjectsPageContainer = universal(() =>
  import(
    /* webpackChunkName: 'ProjectsPageContainer' */ './ProjectsPageContainer'
  )
)
