import { connect } from 'react-redux'
import { initNavigation, navigateTo } from 'actions/environment'

import ProjectsPage from './ProjectsPage'
function mapStateToProps(state, props) {
  const {
    environment: { page, contentId }
  } = state

  return {
    page,
    projectId: contentId
  }
}

const mapDispatchToProps = { initNavigation, navigateTo }
export default connect(mapStateToProps, mapDispatchToProps)(ProjectsPage)
