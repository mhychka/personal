import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'
import config from 'config'

import ProjectHipCatSociety from './projects/ProjectHipCatSociety'
import ProjectChoicely from './projects/ProjectChoicely'
import ProjectChoicelyShare from './projects/ProjectChoicelyShare'
import ProjectChoicelyStudio from './projects/ProjectChoicelyStudio'
import ProjectChoicelyWidget from './projects/ProjectChoicelyWidget'
import ProjectCityOfWonder from './projects/ProjectCityOfWonder'
import ProjectPrintatet from './projects/ProjectPrintatet'
import ProjectToondrive from './projects/ProjectToondrive'

class ProjectsPage extends Component {
  renderProject(id) {
    if (id === 'hipcatsociety') {
      return <ProjectHipCatSociety {...this.props} />
    }
    if (id === 'choicely') {
      return <ProjectChoicely {...this.props} />
    }
    if (id === 'choicely-share') {
      return <ProjectChoicelyShare {...this.props} />
    }
    if (id === 'choicely-studio') {
      return <ProjectChoicelyStudio {...this.props} />
    }
    if (id === 'choicely-widget') {
      return <ProjectChoicelyWidget {...this.props} />
    }
    if (id === 'personal') {
      return <ProjectCityOfWonder {...this.props} />
    }
    if (id === 'printatet') {
      return <ProjectPrintatet {...this.props} />
    }
    if (id === 'toondrive') {
      return <ProjectToondrive {...this.props} />
    }
    if (id === 'cityofwonder') {
      return <ProjectCityOfWonder {...this.props} />
    }

    return null
  }

  render() {
    const { style, projectId, navigateTo } = this.props

    if (projectId) {
      return this.renderProject(projectId)
    }

    return (
      <div className="main-section projects" style={style}>
        <MetaTags>
          <title>Max Hychka | Projects</title>
        </MetaTags>
        <h1 className="main-title">Projects</h1>

        <h3>
          <a onClick={() => navigateTo('projects', 'hipcatsociety')}>
            app.hipcatsociety.com
          </a>
        </h3>
        <h3>
          <a onClick={() => navigateTo('projects', 'hipcatsociety')}>
            admin.hipcatsociety.com
          </a>
        </h3>
        <h3>
          <a onClick={() => navigateTo('projects', 'choicely')}>choicely.com</a>
        </h3>
        <h3>
          <a onClick={() => navigateTo('projects', 'choicely-studio')}>
            studio.choicely.com
          </a>
        </h3>
        <h3>
          <a onClick={() => navigateTo('projects', 'choicely-share')}>
            share.choicely.com
          </a>
        </h3>
        <h3>
          <a onClick={() => navigateTo('projects', 'choicely-widget')}>
            widget.choicely.com
          </a>
        </h3>
        <h3>
          <a href={config.HOST_URL} target="__blank">
            {config.HOST_URL.replace('https://', '')}
          </a>
        </h3>
        <h3>
          <a onClick={() => navigateTo('projects', 'printatet')}>
            print-a-tet.com.ua
          </a>
        </h3>
        <h3>
          <a onClick={() => navigateTo('projects', 'cityofwonder')}>
            facebook.com/cityofwonder
          </a>
        </h3>
        <h3>
          <a onClick={() => navigateTo('projects', 'toondrive')}>
            toondrive.com
          </a>
        </h3>
      </div>
    )
  }
}

export default ProjectsPage
