import React, { Component } from 'react'
import ReactWebMediaPlayer from 'react-web-media-player'
import MetaTags from 'react-meta-tags'

import ModalImage from 'react-modal-image'

class ProjectHipCatSociety extends Component {
  render() {
    const { style, navigateTo } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | HipCatSociety</title>
        </MetaTags>
        <h1 className="main-title">HipCat Society</h1>
        <div>
          <a target="__blank" href="https://app.hipcatsociety.com">
            app.hipcatsociety.com
          </a>
        </div>
        <div>
          <a target="__blank" href="https://admin.hipcatsociety.com">
            admin.hipcatsociety.com
          </a>
        </div>
        <div>
          <a target="__blank" href="https://hipcatsociety.com">
            hipcatsociety.com
          </a>
        </div>
        <p>
          HipCat is your go-to resource for all your internet marketing needs.
          What happens when you mix honesty, authenticity, storytelling, grit,
          and love? A kickass marketer! HipCat Society gives you the education,
          resources, tools, analytics, and inspiration you need to dig deep and
          become that kickass marketer. Connect with your clients and community
          like never before. Simplify the complex world of websites, search
          engine rankings, social marketing, blogging, and much more. Contact
          our marketing experts to learn more about DIY marketing that is sure
          to make your small business stand out above the rest!
        </p>

        <h3>App pages examples</h3>

        <ModalImage
          small="/images/hipcatsociety/1.jpeg"
          large="/images/hipcatsociety/1.jpeg"
        />
        <ModalImage
          small="/images/hipcatsociety/7.jpeg"
          large="/images/hipcatsociety/7.jpeg"
        />
        <ModalImage
          small="/images/hipcatsociety/13.jpeg"
          large="/images/hipcatsociety/13.jpeg"
        />

        <ModalImage
          small="/images/hipcatsociety/3.png"
          large="/images/hipcatsociety/3.png"
        />
        <ModalImage
          small="/images/hipcatsociety/4.jpeg"
          large="/images/hipcatsociety/4.jpeg"
        />
        <ModalImage
          small="/images/hipcatsociety/6.png"
          large="/images/hipcatsociety/6.png"
        />
        <h3>Video about HipCat app and marketing</h3>

        <div style={{ marginLeft: '15px', marginBottom: '20px' }}>
          <ReactWebMediaPlayer
            title="HipCat Society"
            video="/video/hipcatsociety/1.mp4"
            width="100%"
            height="auto"
          />
        </div>

        <h3>Business action planner. Opportunity overview</h3>

        <div style={{ marginLeft: '15px', marginBottom: '20px' }}>
          <ReactWebMediaPlayer
            title="HipCat Society"
            video="/video/hipcatsociety/2.mp4"
            width="100%"
            height="auto"
          />
        </div>

        <h3>App mobile version. Pages examples</h3>

        <ModalImage
          small="/images/hipcatsociety/14.png"
          large="/images/hipcatsociety/14.png"
        />
        <br />
        <br />
        <ModalImage
          small="/images/hipcatsociety/15.png"
          large="/images/hipcatsociety/15.png"
        />
        <br />
        <br />
        <ModalImage
          small="/images/hipcatsociety/16.png"
          large="/images/hipcatsociety/16.png"
        />
        <br />
        <br />
        <ModalImage
          small="/images/hipcatsociety/17.png"
          large="/images/hipcatsociety/17.png"
        />

        <h3>
          Admin web application examples to manage\create content for main app
        </h3>


        <ModalImage
          small="/images/hipcatsociety/5.png"
          large="/images/hipcatsociety/5.png"
        />
        <ModalImage
          small="/images/hipcatsociety/2.png"
          large="/images/hipcatsociety/2.png"
        />
        <ModalImage
          small="/images/hipcatsociety/8.png"
          large="/images/hipcatsociety/8.png"
        />
        <ModalImage
          small="/images/hipcatsociety/9.png"
          large="/images/hipcatsociety/9.png"
        />
        <ModalImage
          small="/images/hipcatsociety/10.png"
          large="/images/hipcatsociety/10.png"
        />
        <ModalImage
          small="/images/hipcatsociety/11.png"
          large="/images/hipcatsociety/11.png"
        />
        <ModalImage
          small="/images/hipcatsociety/12.png"
          large="/images/hipcatsociety/12.png"
        />
      </div>
    )
  }
}

export default ProjectHipCatSociety
