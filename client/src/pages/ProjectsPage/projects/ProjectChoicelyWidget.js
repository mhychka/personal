import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'
import { Widget } from 'components/Widget'
import ModalImage from 'react-modal-image'
import { loadWidgetScript } from 'utils/widget'

class ProjectChoicelyWidget extends Component {
  state = { inited: false }

  componentDidMount() {
    loadWidgetScript(() => this.setState({ inited: true }))
  }

  render() {
    const { style } = this.props
    const { inited } = this.state

    if (!inited) {
      return null
    }

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Choicely Widget</title>
        </MetaTags>
        <h1 className="main-title">Choicely Widget</h1>
        <div>
          <a target="__blank" href="https://studio.choicely.com">
            studio.choicely.com
          </a>
        </div>
        <div>
          <a target="__blank" href="https://facebook.com/choicelyapp">
            facebook.com/choicelyapp
          </a>
        </div>
        <div>
          <a target="__blank" href="https://www.linkedin.com/company/choicely/">
            linkedin.com/company/choicely
          </a>
        </div>
        <p>
          The Choicely Widget is a single page application, which can be embed
          on any web site with embedded code. The app can be created in choicely
          studio app builder by user
        </p>
        <p>I will provide 3 widget exapmles and embed in this site to check</p>
        <h3>1. Voting contest widget</h3>
        <Widget
          contestId="d4964e97-cf6e-11eb-99ef-b7bfb6c99e60"
          params={{ fh: true }}
        />
        <ModalImage
          small="/images/choicely/widget.jpeg"
          large="/images/choicely/widget.jpeg"
        />
        <ModalImage
          small="/images/choicely/widget_1.jpeg"
          large="/images/choicely/widget_1.jpeg"
        />
        <h3>2. Voting video contest football example</h3>
        <Widget
          contestId="59ed0a1e-d053-11eb-9a51-03a026cb4bae"
          params={{ fh: true }}
        />
        <ModalImage
          small="/images/choicely/widget_2.jpeg"
          large="/images/choicely/widget_2.jpeg"
        />
        <ModalImage
          small="/images/choicely/widget_3.jpeg"
          large="/images/choicely/widget_3.jpeg"
        />
        <h3>3. Survey widget example</h3>
        <Widget surveyKey="ag1zfmxvdmVudGVkYXBwciELEgVCcmFuZBiAgIDkjdKVCgwLEgZTdXJ2ZXkY4c3QWAw" />
        <ModalImage
          small="/images/choicely/widget_5.png"
          large="/images/choicely/widget_5.png"
        />
        <ModalImage
          small="/images/choicely/widget_6.png"
          large="/images/choicely/widget_6.png"
        />
        <p>
          So Choicely Widget is a single page app with specific logic like
          contest, survey, feed, article, etc... Content can be editable in
          choicely studio.
        </p>
      </div>
    )
  }
}

export default ProjectChoicelyWidget
