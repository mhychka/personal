import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'
import ReactWebMediaPlayer from 'react-web-media-player'
import ModalImage from 'react-modal-image'

class ProjectChoicelyShare extends Component {
  render() {
    const { style } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Choicely Share</title>
        </MetaTags>
        <h1 className="main-title">Choicely Share</h1>
        <div>
          <a target="__blank" href="https://share.choicely.com">
            share.choicely.com
          </a>
        </div>
        <div>
          <a target="__blank" href="https://facebook.com/choicelyapp">
            facebook.com/choicelyapp
          </a>
        </div>
        <div>
          <a target="__blank" href="https://www.linkedin.com/company/choicely/">
            linkedin.com/company/choicely
          </a>
        </div>
        <p>
          The leading no-code app development platform with inbuilt publishing,
          voting & fan engagement tools
        </p>
        <ReactWebMediaPlayer
          title="Choicely"
          video="/video/choicely/video_4.mp4"
          width="100%"
          height="auto"
        />
        <ReactWebMediaPlayer
          title="Choicely"
          video="/video/choicely/video_5.mp4"
          width="100%"
          height="auto"
        />
        <ModalImage
          small="/images/choicely/share.png"
          large="/images/choicely/share.png"
        />
        <ModalImage
          small="/images/choicely/share_1.jpeg"
          large="/images/choicely/share_1.jpeg"
        />
        <ModalImage
          small="/images/choicely/share_2.jpeg"
          large="/images/choicely/share_2.jpeg"
        />
        <ModalImage
          small="/images/choicely/share_3.jpeg"
          large="/images/choicely/share_3.jpeg"
        />
      </div>
    )
  }
}

export default ProjectChoicelyShare
