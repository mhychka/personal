import React, { Component } from 'react'
import ReactWebMediaPlayer from 'react-web-media-player'
import MetaTags from 'react-meta-tags'

import ModalImage from 'react-modal-image'

class ProjectChoicely extends Component {
  render() {
    const { style, navigateTo } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Choicely</title>
        </MetaTags>
        <h1 className="main-title">Choicely</h1>
        <div>
          <a target="__blank" href="https://choicely.com">
            choicely.com
          </a>
        </div>
        <div>
          <a target="__blank" href="https://facebook.com/choicelyapp">
            facebook.com/choicelyapp
          </a>
        </div>
        <div>
          <a target="__blank" href="https://www.linkedin.com/company/choicely/">
            linkedin.com/company/choicely
          </a>
        </div>
        <p>
          Choicely’s visual mobile app builder platform makes it easy to build
          fully custom mobile apps in a fraction of the time and cost.
        </p>
        <ul>
          <li>Easy to use app builder - No coding required</li>
          <li>Build custom functionality with our developer SDK</li>
          <li>
            Have Choicely design & development team build a world class app for
            your business
          </li>
        </ul>

        <ModalImage
          small="/images/choicely/1.jpeg"
          large="/images/choicely/1.jpeg"
        />

        <div style={{ marginLeft: '15px' }}>
          <ReactWebMediaPlayer
            title="Choicely"
            video="/video/choicely/video.mp4"
            width="100%"
            height="auto"
          />
        </div>
        <p>
          <a onClick={() => navigateTo('projects', 'choicely-studio')}>
            Choicely app builder
          </a>{' '}
          enables kick-starting app projects in a few workshops, and cuts
          project-based budgets of building multi-feature apps from 6-figures to
          an affordable monthly PaaS (Platform-as-a-Service) model. This way
          businesses can go live using a comprehensive feature set, using
          Choicely Studio publishing tools to manage the app and content in real
          time. Any custom native feature can be developed and added to Choicely
          apps.
        </p>
        <ModalImage
          small="/images/choicely/2.png"
          large="/images/choicely/2.png"
        />
      </div>
    )
  }
}

export default ProjectChoicely
