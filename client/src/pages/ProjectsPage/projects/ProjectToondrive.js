import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'
import ModalImage from 'react-modal-image'
import ReactWebMediaPlayer from 'react-web-media-player'

class ProjectToondrive extends Component {
  render() {
    const { style } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Toondrive</title>
        </MetaTags>
        <h1 className="main-title">Toondrive</h1>
        <div>
          <a target="__blank" href="http://toondrive.com">
            toondrive.com
          </a>
        </div>
        <div>
          <a target="__blank" href="https://www.facebook.com/toondrivestudio">
            facebook.com/toondrivestudio
          </a>
        </div>
        <div>
          <a
            target="__blank"
            href="https://www.linkedin.com/company/toondrive/"
          >
            linkedin.com/company/toondrive
          </a>
        </div>
        <p>Web site for animation studio</p>
        <ReactWebMediaPlayer
          title="Toondrive"
          video="/video/toondrive/1.mp4"
          width="100%"
          height="auto"
        />
        <ModalImage
          small="/images/toondrive/1.png"
          large="/images/toondrive/1.png"
        />
        <ModalImage
          small="/images/toondrive/2.jpeg"
          large="/images/toondrive/2.jpeg"
        />
        <ModalImage
          small="/images/toondrive/3.png"
          large="/images/toondrive/3.png"
        />
        <ModalImage
          small="/images/toondrive/4.png"
          large="/images/toondrive/4.png"
        />
      </div>
    )
  }
}

export default ProjectToondrive
