import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'
import ReactWebMediaPlayer from 'react-web-media-player'
import ModalImage from 'react-modal-image'

class ProjectPrintatet extends Component {
  render() {
    const { style } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Printatet</title>
        </MetaTags>
        <h1 className="main-title">Printatet</h1>
        <div>
          <a target="__blank" href="https://print-a-tet.com.ua/">
            print-a-tet.com.ua
          </a>
        </div>
        <div>
          <a target="__blank" href="https://www.facebook.com/printatet">
            facebook.com/printatet
          </a>
        </div>
        <div>
          <a
            target="__blank"
            href="https://www.linkedin.com/company/printatet/"
          >
            linkedin.com/company/print-a-tet
          </a>
        </div>
        <p>
          In Print-a-Tet you can create a design photobook of any format and in
          any binding in a matter of minutes. All your dreams come true right in
          the online editor. Our main goal is to give everyone the opportunity
          to realize themselves as an editor and artist. We help create, print
          and sell really high quality photo books.
        </p>
        <p>
          We do not have a limit on the number of copies of photo books when
          ordering. Order at least one or 101 photo books. Print-a-Tet is
          improving customer service for you every day.
        </p>
        <ReactWebMediaPlayer
          title="Print-a-tet"
          video="/video/printatet/1.mp4"
          width="100%"
          height="auto"
        />
        <ReactWebMediaPlayer
          title="Print-a-tet"
          video="/video/printatet/2.mp4"
          width="100%"
          height="auto"
        />
        <ModalImage
          small="/images/printatet/1.jpg"
          large="/images/printatet/1.jpg"
        />
        <ModalImage
          small="/images/printatet/2.jpg"
          large="/images/printatet/2.jpg"
        />
        <ModalImage
          small="/images/printatet/3.jpg"
          large="/images/printatet/3.jpg"
        />
        <ModalImage
          small="/images/printatet/4.jpg"
          large="/images/printatet/4.jpg"
        />
      </div>
    )
  }
}

export default ProjectPrintatet
