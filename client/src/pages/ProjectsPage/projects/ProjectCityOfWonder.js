import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'
import ReactWebMediaPlayer from 'react-web-media-player'

class ProjectCityOfWonder extends Component {
  render() {
    const { style } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | City Of Wonder</title>
        </MetaTags>
        <h1 className="main-title">City Of Wonder</h1>
        <div>
          <a target="__blank" href="https://facebook.com/choicelyapp">
            facebook.com/cityofwonder
          </a>
        </div>
        <div>
          <a target="__blank" href="https://en.wikipedia.org/wiki/Playdom">
            en.wikipedia.org/wiki/playdom
          </a>
        </div>
        <p>
          City of Wonder was a web browser game produced by Playdom, available
          to all members of Facebook (launched around 17 August 2010). Much of
          it was based on the Civilization series, but there were elements of
          Travian and other games, as well as gifting and other features that
          made use of a player's "friendships," features found in many Facebook
          games.
        </p>
        <p>
          Playdom announced the closure of all online games on its website in
          April 2014, and the studio itself closed in September 2016.
          Consequently, City of Wonder has been discontinued.
        </p>
        <ReactWebMediaPlayer
          title="City Of Wonder"
          video="/video/cityofwonder/1.mp4"
          width="100%"
          height="auto"
        />
      </div>
    )
  }
}

export default ProjectCityOfWonder
