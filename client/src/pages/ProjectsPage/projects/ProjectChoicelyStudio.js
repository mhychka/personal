import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'
import ModalImage from 'react-modal-image'
import ReactWebMediaPlayer from 'react-web-media-player'

class ProjectChoicelyStudio extends Component {
  render() {
    const { style } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Choicely Studio</title>
        </MetaTags>
        <h1 className="main-title">Choicely Studio</h1>
        <div>
          <a target="__blank" href="https://studio.choicely.com">
            studio.choicely.com
          </a>
        </div>
        <div>
          <a target="__blank" href="https://facebook.com/choicelyapp">
            facebook.com/choicelyapp
          </a>
        </div>
        <div>
          <a target="__blank" href="https://www.linkedin.com/company/choicely/">
            linkedin.com/company/choicely
          </a>
        </div>
        <p>
          Choicely’s codeless app development platform enables you to create
          high quality apps without writing a single line of code. Visual
          drag&drop builder is easy to use and you can get going in a matter of
          days in contrast to 6-9 months in custom developing your app.
        </p>
        <ReactWebMediaPlayer
          title="Choicely"
          video="/video/choicely/video_3.mp4"
          width="100%"
          height="auto"
        />
        <ReactWebMediaPlayer
          title="Choicely"
          video="/video/choicely/video_2.mp4"
          width="100%"
          height="auto"
        />
        <ReactWebMediaPlayer
          title="Choicely"
          video="/video/choicely/video_7.mp4"
          width="100%"
          height="auto"
        />
        <ModalImage
          small="/images/choicely/studio_3.png"
          large="/images/choicely/studio_3.png"
        />
        <ModalImage
          small="/images/choicely/studio_11.jpeg"
          large="/images/choicely/studio_11.jpeg"
        />
        <ModalImage
          small="/images/choicely/studio_2.png"
          large="/images/choicely/studio_2.png"
        />
        <ModalImage
          small="/images/choicely/studio_1.png"
          large="/images/choicely/studio_1.png"
        />
        <ModalImage
          small="/images/choicely/studio_6.png"
          large="/images/choicely/studio_6.png"
        />
        <ModalImage
          small="/images/choicely/studio_7.png"
          large="/images/choicely/studio_7.png"
        />
        <ModalImage
          small="/images/choicely/studio_8.png"
          large="/images/choicely/studio_8.png"
        />
        <ModalImage
          small="/images/choicely/studio_9.jpeg"
          large="/images/choicely/studio_9.jpeg"
        />
        <ModalImage
          small="/images/choicely/studio_10.jpeg"
          large="/images/choicely/studio_10.jpeg"
        />
        <ModalImage
          small="/images/choicely/studio_12.png"
          large="/images/choicely/studio_12.jpeg"
        />
        <ModalImage
          small="/images/choicely/studio_13.png"
          large="/images/choicely/studio_13.png"
        />
        <p>
          Choicely is a trusted community for people to explore, join, and vote
          on things that matter to them. Choicely`s studio visual mobile app
          builder platform makes it easy to build fully custom mobile apps
          without coding. Founded in 2014 and headquartered in Helsinki,
          Finland, the company’s mission is to become the most popular voting
          platform in the world.
        </p>
      </div>
    )
  }
}

export default ProjectChoicelyStudio
