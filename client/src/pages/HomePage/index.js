import universal from 'utils/universal'

export const HomePageContainer = universal(() =>
  import(/* webpackChunkName: 'HomePageContainer' */ './HomePageContainer')
)
