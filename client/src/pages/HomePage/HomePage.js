import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'

class HomePage extends Component {
  state = { activeSections: [] }

  toggleSection = activeBlock => {
    const { activeSections } = this.state
    let updatedSections

    if (activeSections.includes(activeBlock)) {
      updatedSections = activeSections.filter(b => b !== activeBlock)
    } else {
      updatedSections = [].concat(activeSections)
      updatedSections.push(activeBlock)
    }

    this.setState({ activeSections: updatedSections })
  }

  renderAboutMe() {
    const { activeSections } = this.state
    const isActive = activeSections.includes('aboutMe')

    return (
      <section id="fh5co-feature-slider">
        <div className="row">
          <div
            className="col-md-6 col-sm-6 col-xs-6 col-xxs-12 fh5co-item-slide-text"
            onClick={() => this.toggleSection('aboutMe')}
          >
            <h2>
              About Me
              <span className="fh5co-border" />
            </h2>
            <div style={{ display: isActive ? 'block' : 'none' }}>
              <p>
                Hey! My name is Max and I am web developer, based in
                Kiev/Ukraine.
              </p>
            </div>
          </div>
        </div>
      </section>
    )
  }

  render() {
    return (
      <div className="main-section">
        <MetaTags>
          <title>Max Hychka | Home</title>
        </MetaTags>
        <div className="home-section">
          <div className="home-section-inner">
            <div className="home-heading">Hi, I'm Max,</div>

            <h2 className="home-subheading">a full stack web developer</h2>
          </div>
        </div>
      </div>
    )
  }
}

export default HomePage
