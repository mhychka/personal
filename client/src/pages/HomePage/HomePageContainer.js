import { connect } from 'react-redux'
import { initNavigation, navigateTo } from 'actions/environment'

import HomePage from './HomePage'

function mapStateToProps(state, props) {
  const {
    environment: { page }
  } = state

  return {
    page
  }
}

const mapDispatchToProps = { initNavigation, navigateTo }
export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
