import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'
import ReactPlayer from 'react-player'

class MusicPage extends Component {
  render() {
    const { style } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Music</title>
        </MetaTags>
        <h1 className="main-title">Music</h1>

        <p>
          Here you can just relax and listen to my some favorite music tracks :)
        </p>

        <h3>Tapolsky near by the South Bridge</h3>
        <div
          style={{
            marginLeft: '15px',
            marginTop: '10px',
            marginBottom: '10px'
          }}
        >
          <ReactPlayer
            width="100%"
            url="https://www.youtube.com/watch?v=Ezw9O5LKIZM"
            controls={true}
          />
        </div>

        <h3>Tapolsky on Bakota</h3>
        <div
          style={{
            marginLeft: '15px',
            marginTop: '10px',
            marginBottom: '10px'
          }}
        >
          <ReactPlayer
            width="100%"
            url="https://www.youtube.com/watch?v=LxgCWBkpLkQ"
            controls={true}
          />
        </div>

        <h3>Tapolsky on Dzharylgach island</h3>
        <div
          style={{
            marginLeft: '15px',
            marginTop: '10px',
            marginBottom: '10px'
          }}
        >
          <ReactPlayer
            width="100%"
            url="https://www.youtube.com/watch?v=jSCcLrqZn6Y"
            controls={true}
          />
        </div>

        <h3>Tapolsky Birthday Mix</h3>
        <div
          style={{
            marginLeft: '15px',
            marginTop: '10px',
            marginBottom: '10px'
          }}
        >
          <ReactPlayer
            width="100%"
            url="https://www.youtube.com/watch?v=kvlJ4U-zmL4"
            controls={true}
          />
        </div>

        <h3>Tapolsky on a roof</h3>
        <div
          style={{
            marginLeft: '15px',
            marginTop: '10px',
            marginBottom: '10px'
          }}
        >
          <ReactPlayer
            width="100%"
            url="https://www.youtube.com/watch?v=6rbt7lKxwcA"
            controls={true}
          />
        </div>

        <h3>The last event of summer</h3>
        <div
          style={{
            marginLeft: '15px',
            marginTop: '10px',
            marginBottom: '10px'
          }}
        >
          <ReactPlayer
            width="100%"
            url="https://www.youtube.com/watch?v=TQnknJP3a8A"
            controls={true}
          />
        </div>
      </div>
    )
  }
}

export default MusicPage
