import universal from 'utils/universal'

export const MusicPageContainer = universal(() =>
  import(/* webpackChunkName: 'MusicPageContainer' */ './MusicPageContainer')
)
