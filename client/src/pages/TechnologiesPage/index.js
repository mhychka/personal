import universal from 'utils/universal'

export const TechnologiesPageContainer = universal(() =>
  import(
    /* webpackChunkName: 'TechnologiesPageContainer' */ './TechnologiesPageContainer'
  )
)
