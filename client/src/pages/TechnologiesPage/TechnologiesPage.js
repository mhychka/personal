import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'

class TechnologiesPage extends Component {
  render() {
    const { style } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Technologies</title>
        </MetaTags>
        <h1 className="main-title">Technologies</h1>
        <h2 className="red">
          <i className="fa fa-edit" /> Front-end
        </h2>
        <ul>
          <li>
            In projects I prefer to use <strong>TypeScript</strong> or{' '}
            <strong>EcmaScript</strong> are compiled by <strong>Webpack</strong>
            . Most of single page applications was developed with{' '}
            <strong>ReactJS</strong>, using such libs as react-intl, redux-form,
            react-select, etc...
          </li>
          <li>
            I like to use <strong>webpack code splitting</strong>. It improves
            application perfomance and let a component to be loaded on demand,
            not in main app bundle from start. It makes app js bundles small and
            fast to load.
          </li>
          <li>
            I prefer to write <strong>pretty, small and clean</strong> react
            components, so that every developer can understand and support an
            app logic
          </li>
          <li>
            To keep code clean and pretty use{' '}
            <strong>eslint and stylelint</strong>
          </li>
        </ul>
        <h2 className="red">
          <i className="fa fa-code" /> Back-end
        </h2>
        <ul>
          <li>
            Use <strong>NodeJs</strong>, <strong>ExpressJs</strong>,{' '}
            <strong>Koa</strong>, <strong>TypeORM</strong> to develop or support
            backend servers
          </li>
          <li>
            I have experience in development html{' '}
            <strong>server side rendering</strong> for social network sharing
            and SEO web site optimization.
          </li>
          <li>
            Is able to handle <strong>REST API</strong> requests from various
            app clients
          </li>
          <li>
            Is able to work and set up <strong>GraphQL</strong> requests
          </li>
          <li>
            For hot updates in application use <strong>WebSockets</strong> or{' '}
            <strong>Google Firebase</strong>
          </li>
          <li>
            Is able to set and configure <strong>apache or nginx</strong> server
          </li>
          <li>
            For running shedule tasks use{' '}
            <strong>Cron or Google Cloud Functions</strong>
          </li>
        </ul>
        <h2 className="red">
          <i className="fa fa-terminal" /> Databases
        </h2>
        <ul>
          <li>
            Prefer to use <strong>PostgreSQL</strong> to work with data and work
            with <strong>SQL</strong> query optimization
          </li>
          <li>
            Had worked with <strong>Google BigQuery</strong> and optimized
            application for highload processes
          </li>
          <li>
            Had tasks linked with <strong>Redis</strong> and similar key-value
            in-memory data structure stores
          </li>
        </ul>
      </div>
    )
  }
}

export default TechnologiesPage
