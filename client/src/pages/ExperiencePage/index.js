import universal from 'utils/universal'

export const ExperiencePageContainer = universal(() =>
  import(
    /* webpackChunkName: 'ExperiencePageContainer' */ './ExperiencePageContainer'
  )
)
