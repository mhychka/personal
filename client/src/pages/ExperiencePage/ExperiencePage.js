import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'
import ModalImage from 'react-modal-image'

class ExperiencePage extends Component {
  render() {
    const { style, navigateTo } = this.props

    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Experience</title>
        </MetaTags>
        <h1 className="main-title">Experience</h1>

        <div className="timeline portfolio-timeline">
          <ul>
            <li className="timeline_element timeline_element--now project">
              <div className="timeline_element-date">
                <time className="timeline_element-date-text">Now</time>
              </div>

              <div className="timeline_element-contents">
                <div className="project-text">
                  <div className="project-description">
                    Currently available
                    <br />
                    Looking for a full-stack developer position willing to join
                    your team
                  </div>
                </div>
              </div>
            </li>

            <li className="timeline_element project">
              <div className="timeline_element-date">
                <time className="timeline_element-date-text">2021 - 2022</time>
              </div>

              <div className="timeline_element-contents">
                <div className="project-text">
                  <div className="project-title">
                    Team Lead Full Stack Developer at{' '}
                    <a href="https://hipcatsociety.com" target="__blank">
                      HipCat Society
                    </a>
                  </div>

                  <div className="project-subtitle">
                    Marketing, Online Marketing, Social Marketing
                  </div>
                  <div className="project-description">
                    <p>
                      HipCat is your go-to resource for all your internet
                      marketing needs. What happens when you mix honesty,
                      authenticity, storytelling, grit, and love? A kickass
                      marketer! HipCat Society gives you the education,
                      resources, tools, analytics, and inspiration you need to
                      dig deep and become that kickass marketer. Connect with
                      your clients and community like never before. Simplify the
                      complex world of websites, search engine rankings, social
                      marketing, blogging, and much more. Contact our marketing
                      experts to learn more about DIY marketing that is sure to
                      make your small business stand out above the rest!
                    </p>
                    <div>
                      <a
                        href="https://app.hipcatsociety.com"
                        alt="HipCat App"
                        target="__blank"
                      >
                        app.hipcatsociety.com
                      </a>
                    </div>
                    <div>
                      <a
                        href="https://admin.hipcatsociety.com"
                        alt="HipCat Admin"
                        target="__blank"
                      >
                        admin.hipcatsociety.com
                      </a>
                    </div>
                    <div>
                      <a
                        href="https://hipcatsociety.com"
                        alt="HipCat Site"
                        target="__blank"
                      >
                        hipcatsociety.com
                      </a>
                    </div>
                    <p>Achievements:</p>
                    <ul>
                      <li>
                        Successfully developed company`s application –{' '}
                        <a
                          target="__blank"
                          href="https://app.hipcatsociety.com"
                        >
                          app.hipcatsociety.com
                        </a>
                        .
                      </li>
                      <li>
                        Successfully developed company`s admin application{' '}
                        <a
                          target="__blank"
                          href="https://admin.hipcatsociety.com"
                        >
                          admin.hipcatsociety.com
                        </a>
                        .
                      </li>
                      <li>
                        Successfully developed company`s backend api server{' '}
                        <a
                          target="__blank"
                          href="https://api.hipcatsociety.com"
                        >
                          api.hipcatsociety.com
                        </a>
                        .
                      </li>
                      <li>
                        Successfully developed flexible architecture of backend
                        and frontend projects.
                      </li>
                      <li>
                        My diverse technical background has allowed me to
                        deliver exceptional solutions and manage a
                        high-performing team effectively.
                      </li>
                      <li>
                        Played a key role throughout the software development
                        and support life cycle of projects to ensure that
                        quality solutions meet business objectives.
                      </li>
                      <li>
                        Improved the products, finding excellent and simple
                        solutions to implement.
                      </li>
                      <li>
                        Accumulated a profound understanding of complex product
                        development including the design process, development
                        process lifecycle, testing, and delivery.
                      </li>
                    </ul>
                  </div>
                  <a
                    onClick={() => navigateTo('projects', 'hipcatsociety')}
                    className="project-readmore button button-red"
                  >
                    More Info
                  </a>

                  <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                      <li>
                        <a
                          href="https://nodejs.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          NodeJs
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.typescriptlang.org"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Typescript
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://typeorm.io"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          TypeORM
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://reactjs.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          React
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://graphql.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          GraphQL
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://webpack.js.org"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Webpack
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.postgresql.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          PostgreSQL
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.docker.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Docker
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://aws.amazon.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          AWS
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="project-image">
                  <ModalImage
                    small="/images/hipcatsociety.png"
                    large="/images/hipcatsociety.png"
                    alt="HipCat Society Project"
                  />
                </div>
              </div>
            </li>

            <li className="timeline_element project">
              <div className="timeline_element-date">
                <time className="timeline_element-date-text">2016 - 2021</time>
              </div>

              <div className="timeline_element-contents">
                <div className="project-text">
                  <div className="project-title">
                    Senior Full Stack Developer at{' '}
                    <a href="https://choicely.com" target="__blank">
                      Choicely
                    </a>
                  </div>

                  <div className="project-subtitle">
                    Choicely is a popular voting platform and visual mobile app
                    builder
                  </div>
                  <div className="project-description">
                    <p>
                      Studio is a codeless app development platform that enables
                      building high quality native apps.{' '}
                    </p>
                    <p>
                      Choicely Studio publishing tool provides a visual,
                      easy-to-use drag & drop builder for building apps - no
                      coding skills needed. The platform enables building apps
                      with extensive native features, and dropping in any
                      web-based services in minutes. The platform also enables
                      integrating content from social medias and video
                      platforms.
                    </p>
                    <p>
                      Choicely platform is used by leaders in media, sports,
                      entertainment and also used for e-commerce, political
                      parties and resorts. Choicely app builder enables
                      kick-starting app projects in a few workshops, and cuts
                      project-based budgets of building multi-feature apps from
                      6-figures to an affordable monthly PaaS
                      (Platform-as-a-Service) model. This way businesses can go
                      live using a comprehensive feature set, using Choicely
                      Studio publishing tools to manage the app and content in
                      real time. Any custom native feature can be developed and
                      added to Choicely apps.
                    </p>
                    <div>
                      <a
                        href="https://choicely.com"
                        alt="Choicely Project"
                        target="__blank"
                      >
                        choicely.com
                      </a>
                    </div>
                    <div>
                      <a
                        href="https://studio.choicely.com"
                        alt="Choicely Studio Project"
                        target="__blank"
                      >
                        studio.choicely.com
                      </a>
                    </div>
                    <div>
                      <a
                        href="https://share.choicely.com"
                        alt="Share Choicely Project"
                        target="__blank"
                      >
                        share.choicely.com
                      </a>
                    </div>
                    <p>Achievements:</p>
                    <ul>
                      <li>
                        Participated in creating{' '}
                        <a target="__blank" href="https://choicely.com">
                          choicely.com
                        </a>{' '}
                        main site to represent{' '}
                        <a
                          target="__blank"
                          href="https://choicely.com/app-templates"
                        >
                          mobile app examples
                        </a>
                        , which can be build with choicely{' '}
                        <a target="__blank" href="https://studio.choicely.com">
                          studio app builder
                        </a>
                      </li>
                      <li>
                        Successfully developed{' '}
                        <a
                          onClick={() =>
                            navigateTo('projects', 'choicely-widget')
                          }
                        >
                          Choicely Widget
                        </a>
                        , Single Page Application, which can be built to any web
                        site via embed code to show choicely content like
                        contest, article, survey, application, feed. So that
                        users can create\manage their own content with specific
                        logic in{' '}
                        <a target="__blank" href="https://studio.choicely.com">
                          studio app builder
                        </a>{' '}
                        and add it to websites without coding. The Widget
                        contains social network sharing tools, so that user may
                        share own website page with widget immediately after
                        embedding
                      </li>
                      <li>
                        Developed with a team{' '}
                        <a target="__blank" href="https://studio.choicely.com">
                          Choicely Studio
                        </a>{' '}
                        project. The project provides tools for creating and
                        managing content for websites and mobile apps. Users may
                        create a{' '}
                        <a
                          target="__blank"
                          href="https://studio.choicely.com/contest/create"
                        >
                          contest for voting
                        </a>
                        , a news feed, article, survey form to collect and
                        handling information, or even a mobile app. All content
                        can be editable in a minute
                      </li>

                      <li>
                        Set and configure most of choicely projects and helping
                        in deployment processes using Docker, Google console and
                        other tools
                      </li>
                      <li>
                        Implemented{' '}
                        <a
                          target="__blank"
                          href="https://auth.choicely.com/app?v=login&am=facebook,google,email"
                        >
                          auth.choicely.com
                        </a>{' '}
                        microservice which provides tools for sign-in/sign-up
                        processes in choicely web projects
                      </li>
                      <li>
                        Develop choicely stats server and Rest Api which
                        provides voting\revenue data of choicely contest
                        dashboard. It calculates votes, and keep data in
                        PostgreSQL database
                      </li>
                    </ul>
                  </div>
                  <a
                    onClick={() => navigateTo('projects', 'choicely')}
                    className="project-readmore button button-red"
                  >
                    More Info
                  </a>

                  <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                      <li>
                        <a
                          href="https://www.w3schools.com/JS/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Javascript
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://nodejs.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          NodeJs
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://reactjs.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          React
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://redux.js.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Redux
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://webpack.js.org"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Webpack
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.postgresql.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          PostgreSQL
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.restapitutorial.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          REST API
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://firebase.google.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Firebase
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://cloud.google.com/bigquery"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          BigQuery
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.docker.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Docker
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="project-image">
                  <ModalImage
                    small="/images/choicely.png"
                    large="/images/choicely.png"
                    alt="Choicely Project"
                  />
                </div>
              </div>
            </li>

            <li className="timeline_element project">
              <div className="timeline_element-date">
                <time className="timeline_element-date-text">2015 - 2016</time>
              </div>

              <div className="timeline_element-contents">
                <div className="project-text">
                  <div className="project-title">
                    Senior Full Stack Developer at{' '}
                    <a href="http://cobase.se" target="__blank">
                      Cobase
                    </a>
                  </div>
                  <div className="project-subtitle">
                    <p>
                      Cobase is a collaboration app between construction
                      companies.
                    </p>
                  </div>

                  <div className="project-description">
                    <p>
                      CoBase is a cross platfrom HTML5 application, that enables
                      you to carrie information everywhere. CoBase will change
                      how people communicate in construction industry. This
                      digital platform will speed up construction by helping
                      people to plan the work and organize every day processes.
                      CoBase makes collaboration between construction companies
                      easy and productive.
                    </p>
                    <p>
                      <a
                        href="https://cobase.se"
                        alt="Cobase Project"
                        target="__blank"
                      >
                        cobase.se
                      </a>
                    </p>
                    <p>Achievements:</p>
                    <ul>
                      <li>
                        Successfully brought the application app.cobase.se from
                        idea to release as main lead developer.
                      </li>
                      <li>
                        Successfully developed Rest API server for app.cobase.se
                        application.
                      </li>
                      <li>
                        Used AWS technologies to run an API server, support, and
                        scale it.
                      </li>
                      <li>
                        Negotiated directly with a stakeholder and front-end
                        developer to set up requirements for the software.
                      </li>
                      <li>
                        Maintained existing services and pipelines, implemented
                        new features from scratch.
                      </li>
                    </ul>
                  </div>

                  <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                      <li>
                        <a
                          href="https://www.w3schools.com/JS/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Javascript
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://reactjs.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          React
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://redux.js.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Redux
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://webpack.js.org"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Webpack
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.postgresql.org/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          PostgreSQL
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.restapitutorial.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          REST API
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.docker.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Docker
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="project-image">
                  <ModalImage
                    small="/images/cobase.png"
                    large="/images/cobase.png"
                    alt="Cobase Project"
                  />
                </div>
              </div>
            </li>

            <li className="timeline_element project">
              <div className="timeline_element-date">
                <time className="timeline_element-date-text">2011 - 2015</time>
              </div>

              <div className="timeline_element-contents">
                <div className="project-text">
                  <div className="project-title">
                    Middle PHP Developer at{' '}
                    <a href="https://print-a-tet.com.ua" target="__blank">
                      Print-a-tet
                    </a>
                  </div>
                  <div className="project-subtitle">
                    <p>Print-a-tet is a photobook printing online builder</p>
                  </div>

                  <div className="project-description">
                    <p>
                      The project Print-a-tet ​​is a foreign investment project.
                      The development and implementation of online services,
                      actively using HTML5 technology and designed for heavy
                      loads. Company ​​pays special attention, is given to the
                      qualitative development of design and design online
                      printing - from photo books and business printing up to
                      souvenirs and personal printing.
                    </p>
                    <p>
                      <a
                        href="http://print-a-tet.com.ua"
                        alt="Print-a-tet"
                        target="__blank"
                      >
                        print-a-tet.com.ua
                      </a>
                    </p>
                    <p>Achievements:</p>
                    <ul>
                      <li>
                        Successfully developed the company`s website
                        print-a-tet.com.ua
                      </li>
                      <li>
                        Took part in developing javascript editor
                        print-a-tet.com.ua/editor for live creating of
                        photobooks and postcards on the website to order
                      </li>
                      <li>
                        Developed website`s print-a-tet.com.ua orders logic and
                        order statuses
                      </li>
                      <li>
                        Refactored features with slow response time to be more
                        performant.
                      </li>
                      <li>
                        Successfully developed Rest Api server for
                        print-a-tet.com.ua/editor
                      </li>
                      <li>Implemented new features from scratch</li>
                    </ul>
                  </div>

                  <a
                    onClick={() => navigateTo('projects', 'printatet')}
                    className="project-readmore button button-red"
                  >
                    More Info
                  </a>

                  <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                      <li>
                        <a
                          href="https://www.php.net/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          PHP
                        </a>
                      </li>
                      <li>
                        <a
                          href="https://www.w3schools.com/JS/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Javascript
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://jquery.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Jquery
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.mysql.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          MySQL
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.restapitutorial.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          REST API
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="project-image">
                  <ModalImage
                    small="/images/printatet.jpg"
                    large="/images/printatet.jpg"
                    alt="Print-a-tet Project"
                  />
                </div>
              </div>
            </li>

            <li className="timeline_element project">
              <div className="timeline_element-date">
                <time className="timeline_element-date-text">2011 - 2011</time>
              </div>

              <div className="timeline_element-contents">
                <div className="project-text">
                  <div className="project-title">
                    PHP Web Developer at{' '}
                    <a
                      href="https://www.linkedin.com/company/nikitova-llc/about/"
                      target="__blank"
                    >
                      Nikitova LLC
                    </a>
                  </div>
                  <div className="project-subtitle">
                    <p>
                      Nikitova is an international full game development
                      outsourcing company
                    </p>
                  </div>

                  <div className="project-description">
                    <p>
                      Nikitova LLC was founded in 2000. It has team of 170+
                      specialists with 300+ art & engineering outsourcing
                      projects completed, & 7+ fully developed titles released.
                      The company maintains a diverse portfolio of products that
                      span a wide range of categories and target markets that
                      can be used on a variety of game platforms such as:
                      Nintendo Wii, DS, PC, Xbox 360, PS3 and variety of mobile
                      devices (iOS and Android).
                    </p>
                    <p>
                      <a
                        href="https://facebook.com/cityofwonder"
                        alt="City Of Wonder Game"
                        target="__blank"
                      >
                        facebook.com/cityofwonder
                      </a>
                    </p>
                    <p>Achievements:</p>
                    <ul>
                      <li>
                        Collaborated with a team and successfully developed a
                        javascript game - the City Of Wonder for Facebook and
                        Google Orkut social networks.
                      </li>
                      <li>
                        Created membership, authorization, payment features, and
                        maintained them during the project's lifetime.
                      </li>
                      <li>
                        Working with Facebook and Orkut API closely to port the
                        game to social network.
                      </li>
                      <li>
                        Implemented social network sharing information and
                        player`s game results among social network users.
                      </li>
                    </ul>
                  </div>

                  <a
                    onClick={() => navigateTo('projects', 'cityofwonder')}
                    className="project-readmore button button-red"
                  >
                    More Info
                  </a>

                  <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                      <li>
                        <a
                          href="https://www.php.net/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          PHP
                        </a>
                      </li>
                      <li>
                        <a
                          href="https://www.w3schools.com/JS/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Javascript
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://jquery.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Jquery
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.mysql.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          MySQL
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.restapitutorial.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          REST API
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="project-image">
                  <ModalImage
                    small="/images/cityofwonder.jpeg"
                    large="/images/cityofwonder.jpeg"
                    alt="City Of Wonder Project"
                  />
                </div>
              </div>
            </li>

            <li className="timeline_element project">
              <div className="timeline_element-date">
                <time className="timeline_element-date-text">2008 - 2011</time>
              </div>

              <div className="timeline_element-contents">
                <div className="project-text">
                  <div className="project-title">
                    PHP Web Developer at{' '}
                    <a
                      href="https://www.linkedin.com/company/nikitova-llc/about/"
                      target="__blank"
                    >
                      WebOS
                    </a>
                  </div>
                  <div className="project-subtitle">
                    <p>Web studio</p>
                  </div>

                  <div className="project-description">
                    <p>
                      The WebOS studio is the company, which strive to keep the
                      customers happy! When your business depends on website
                      performance and reliability, you need a website that team
                      can deliver. It has team of 4+ specialists. The Studio is
                      equipped to make that happen with ease and efficiency
                    </p>
                    <p>
                      <a
                        href="http://toondrive.com/"
                        alt="Toon Drive Project"
                        target="__blank"
                      >
                        toondrive.com
                      </a>
                    </p>
                    <p>Achievements:</p>
                    <ul>
                      <li>
                        Successfully developed websites for clients according to
                        web design and business objectives.
                      </li>
                      <li>
                        Worked on a private company`s PHP framework engine, with
                        which the websites were made.
                      </li>
                      <li>
                        Supported existing websites, PHP engine, added new
                        features and improved performance.
                      </li>
                    </ul>
                  </div>

                  <a
                    onClick={() => navigateTo('projects', 'toondrive')}
                    className="project-readmore button button-red"
                  >
                    More Info
                  </a>

                  <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                      <li>
                        <a
                          href="https://www.php.net/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          PHP
                        </a>
                      </li>
                      <li>
                        <a
                          href="https://www.w3schools.com/JS/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Javascript
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://jquery.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          Jquery
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.mysql.com/"
                          rel="nofollow noreferrer"
                          target="_blank"
                        >
                          MySQL
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="project-image">
                  <ModalImage
                    small="/images/toondrive.png"
                    large="/images/toondrive.png"
                    alt="Toon Drive Project"
                  />
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    )
  }
}

export default ExperiencePage
