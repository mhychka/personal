import universal from 'utils/universal'

export const ConditionsPageContainer = universal(() =>
  import(
    /* webpackChunkName: 'ConditionsPageContainer' */ './ConditionsPageContainer'
  )
)
