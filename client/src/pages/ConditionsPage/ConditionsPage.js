import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'

class ConditionsPage extends Component {
  render() {
    const { style } = this.props
    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Conditions</title>
        </MetaTags>
        <h1 className="main-title">Conditions</h1>

        <p>
          The list of my personal working situation and relations with the
          employer
        </p>
        <ul>
          <li>Remote work of 80-90 hours per month.</li>
          <li>
            Usually dont work on saturdays(religion). Only asap issues and
            critical hotfixes if needed.
          </li>
          <li>
            Dont work on The Twelve Great Feasts like Christmas, Theophany,
            etc...
          </li>
          <li>
            8 vacation paid hours per every 80 working hours. Usually 1 vacation
            day in month.
          </li>
          <li>
            I am not a CSS developer. I prefer to work with core app logic and
            processes. Usually I work with CSS Layout developer in a team. But I
            understand CSS and can make changes if needed.
          </li>
        </ul>
      </div>
    )
  }
}

export default ConditionsPage
