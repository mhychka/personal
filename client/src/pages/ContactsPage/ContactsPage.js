import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'

class ContactsPage extends Component {
  render() {
    const { style } = this.props
    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Contacts</title>
        </MetaTags>
        <h1 className="main-title">Contacts</h1>

        <div className="vcard">
          <dl className="dl dl-vertical">
            <dt>E-mail:</dt>
            <dd>
              <a href="mailto:max.hychka@gmail.com" target="__blank">
                <i className="icon fa fa-envelope" /> max.hychka@gmail.com
              </a>
            </dd>

            <dt>LinkedIn:</dt>
            <dd>
              <a href="https://www.linkedin.com/in/mhychka" target="__blank">
                <i className="icon fa fa-linkedin" /> mhychka
              </a>
            </dd>
          </dl>
        </div>
      </div>
    )
  }
}

export default ContactsPage
