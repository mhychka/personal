import universal from 'utils/universal'

export const ContactsPageContainer = universal(() =>
  import(
    /* webpackChunkName: 'ContactsPageContainer' */ './ContactsPageContainer'
  )
)
