import { connect } from 'react-redux'
import { initNavigation, navigateTo } from 'actions/environment'

import CodeExamplesPage from './CodeExamplesPage'

function mapStateToProps(state, props) {
  const {
    environment: { page }
  } = state

  return {
    page
  }
}

const mapDispatchToProps = { initNavigation, navigateTo }
export default connect(mapStateToProps, mapDispatchToProps)(CodeExamplesPage)
