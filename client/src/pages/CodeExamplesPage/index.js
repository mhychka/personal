import universal from 'utils/universal'

export const CodeExamplesPageContainer = universal(() =>
  import(
    /* webpackChunkName: 'CodeExamplesPageContainer' */ './CodeExamplesPageContainer'
  )
)
