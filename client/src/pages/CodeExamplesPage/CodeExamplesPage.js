import React, { Component } from 'react'
import MetaTags from 'react-meta-tags'

class CodeExamplesPage extends Component {
  render() {
    const { style } = this.props
    return (
      <div className="main-section" style={style}>
        <MetaTags>
          <title>Max Hychka | Code Examples</title>
        </MetaTags>
        <h1 className="main-title">Code Examples</h1>

        <p>Check my source code examples</p>
        <p>
          <a target="__blank" href="https://bitbucket.org/mhychka/personal">
            bitbucket.org/mhychka/personal
          </a>
        </p>
        <p>
          <a target="__blank" href="https://bitbucket.org/mhychka/backend-task">
            bitbucket.org/mhychka/backend-task
          </a>
        </p>
        <p>
          <a target="__blank" href="https://jsfiddle.net/vqy6sLx1">
            jsfiddle.net/vqy6sLx1
          </a>
        </p>
      </div>
    )
  }
}

export default CodeExamplesPage
