import * as types from 'constants/action-types'

const initialState = {
  page: 'home',
  contentId: null
}

export default function environment(previousState = initialState, action) {
  switch (action.type) {
    case types.ENVIRONMENT_PAGE_CHANGED:
      return Object.assign({}, previousState, {
        page: action.page,
        contentId: action.contentId
      })
    default:
      return previousState
  }
}
