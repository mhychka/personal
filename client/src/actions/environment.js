import * as types from '../constants/action-types'
import { parseQueryParams } from 'utils/url'

function initNavigationParams() {
  const params = parseQueryParams(window.document.location.search)
  const page = params.page || 'home'
  const contentId = params.cid || null

  return {
    type: types.ENVIRONMENT_PAGE_CHANGED,
    page,
    contentId
  }
}
export function initNavigation() {
  return dispatch => {
    window.addEventListener('popstate', () => dispatch(initNavigationParams()))
    dispatch(initNavigationParams())
  }
}

export function navigateTo(page, contentId = null) {
  if (!page) {
    return false
  }

  let pageUrl = `/?page=${page}`
  if (contentId) {
    pageUrl += `&cid=${contentId}`
  }

  const url = page === 'home' ? '/' : pageUrl

  window.history.pushState({ page }, page, url)

  setTimeout(() => window.scrollTo(0, 0), 400)

  return {
    type: types.ENVIRONMENT_PAGE_CHANGED,
    page,
    contentId
  }
}
