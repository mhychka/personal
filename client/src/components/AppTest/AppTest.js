import React, { Component } from 'react'
import request from 'request'
const https = require('https')

// https.get('https://encrypted.google.com/', function(res) {
//   console.log("statusCode: ", res.statusCode);
//   console.log("headers: ", res.headers);
//
//   res.on('data', function(d) {
//     process.stdout.write(d);
//   });
//
// }).on('error', function(e) {
//   console.error(e);
// });

class AppTest extends Component {
  componentWillMount() {
    const { initNavigation } = this.props
    initNavigation()
  }

  renderMaximumTrailing(levels) {
    const levelsMaxLength = 2 * Math.pow(10, 5)
    const levelsLength = levels.length
    const levelMinLength = -Math.pow(10, 6)
    const levelMaxLength = Math.pow(10, 6)
    let maxTrailing = -1

    if (
      !levels ||
      !levels.length ||
      levels.length <= 1 ||
      levels.length >= levelsMaxLength
    ) {
      throw new Error('Invalid levels data array')
    }

    for (let i = 0; i < levelsLength; i++) {
      const level = levels[i]

      if (
        typeof level !== 'number' ||
        level < levelMinLength ||
        level > levelMaxLength
      ) {
        throw new Error('Invalid level value in levels data array.')
      }

      const prevLevels = levels.slice(0, i).filter(l => l < level)

      if (prevLevels.length) {
        const minLevel = Math.min(...prevLevels)
        const trailing = level - minLevel

        if (trailing > maxTrailing) {
          maxTrailing = trailing
        }
      }
    }

    return maxTrailing
  }

  async loadMovies(substr, page = null) {
    return new Promise((resolve, reject) => {
      let url = `https://jsonmock.hackerrank.com/api/movies/search?Title=${substr}`

      if (page) {
        url += `&page=${page}`
      }

      return https
        .get(url, res => {
          var body = ''

          res.on('data', chunk => {
            body += chunk
          })

          res.on('end', function() {
            const data = JSON.parse(body)
            resolve(data)
          })
        })
        .on('error', function(e) {
          console.error(e)
        })
    })
  }

  async loadAllMovies(substr) {
    return new Promise(async (resolve, reject) => {
      let allMovies = []
      const movies = await this.loadMovies(substr)
      const pagesCount = movies.total_pages || 1

      allMovies = allMovies.concat(movies.data)

      if (pagesCount === 1) {
        resolve(allMovies)
      }

      for (let p = 2; p <= pagesCount; p++) {
        const movies = await this.loadMovies(substr, p)
        allMovies = allMovies.concat(movies.data)
      }

      resolve(allMovies)
    })
  }

  async getMovieTitles(substr) {
    const movies = await this.loadAllMovies(substr)
    movies.sort((a, b) => {
      const aValue = a.imdbID.replace(/[^0-9]+/g, '')
      const bValue = b.imdbID.replace(/[^0-9]+/g, '')

      return aValue - bValue
    })

    const movieTitles = movies.map(m => m.Title + ' ' + m.imdbID)

    return movieTitles
  }

  render() {
    this.getMovieTitles('spiderman')
    return <div>test</div>
  }
}

export default AppTest
