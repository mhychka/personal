import React, { Component } from 'react'
import { activeClass } from 'utils/url'

class AppLayout extends Component {
  state = { mobileMenu: false, minHeight: 0 }

  componentDidMount() {
    this.setMinHeight()
  }

  componentWillReceiveProps(nextProps) {
    const { page } = nextProps
    if (page !== this.props.page) {
      this.setState({ mobileMenu: false })
    }
  }

  setMinHeight() {
    if (this.sidebar && this.sidebar.clientHeight) {
      this.setState({ minHeight: this.sidebar.clientHeight })
    }
  }

  toggleMobileMenu = () => {
    this.setState({ mobileMenu: !this.state.mobileMenu })
  }

  renderLeftPanel() {
    const { page, navigateTo } = this.props
    const { mobileMenu } = this.state

    return (
      <aside className="site-sidebar" ref={ref => (this.sidebar = ref)}>
        <div className="site-sidebar-inner h-card">
          <a
            onClick={() => navigateTo('home')}
            rel="me"
            className="person u-url"
            style={{ cursor: 'pointer' }}
          >
            <div className="person-avatar u-photo">
              <img alt="Photo" src="/images/photo.jpeg" className="avatar" />
            </div>
            <div className="person-content">
              <h1 className="person-title p-name">Max Hychka</h1>
              <h2 className="person-subtitle p-job-title p-note">
                Full Stack Web Developer
              </h2>
            </div>
          </a>

          <nav
            className={`block main-navigation ${mobileMenu ? 'extended' : ''}`}
          >
            <div className="navigation-extend-bar">
              <div className="social-icons">
                <a
                  rel="me"
                  href="mailto:max.hychka@gmail.com"
                  className="button button-icon u-email"
                  title="email: max.hychka@gmail.com"
                >
                  <i className="fa fa-envelope" />
                </a>

                <a
                  rel="me"
                  href="https://www.linkedin.com/in/mhychka"
                  className="button button-icon"
                  title="linkedin: mhychka"
                >
                  <i className="fa fa-linkedin-square" />
                </a>

                <a
                  rel="me"
                  href="https://facebook.com/mhychka"
                  className="button button-icon"
                  title="facebook: mhychka"
                >
                  <i className="fa fa-facebook-square" />
                </a>
              </div>

              <a
                onClick={() => this.toggleMobileMenu()}
                className="navigation-extend-button js-extend-main-navigation"
              >
                <i className="fa fa-bars" />
              </a>
            </div>
            <div className="navigation-extendable">
              <ul>
                <li className={activeClass('about', page)}>
                  <a onClick={() => navigateTo('about')}>About Me</a>
                </li>
                <li className={activeClass('experience', page)}>
                  <a onClick={() => navigateTo('experience')}>Experience</a>
                </li>
                <li className={activeClass('technologies', page)}>
                  <a onClick={() => navigateTo('technologies')}>Technologies</a>
                </li>

                <li className={activeClass('projects', page)}>
                  <a onClick={() => navigateTo('projects')}>Projects</a>
                </li>
                <li className={activeClass('code', page)}>
                  <a onClick={() => navigateTo('code')}>Code examples</a>
                </li>
                <li className={activeClass('philosophy', page)}>
                  <a onClick={() => navigateTo('philosophy')}>Philosophy</a>
                </li>
                <li className={activeClass('music', page)}>
                  <a onClick={() => navigateTo('music')}>Music</a>
                </li>
              </ul>
              <ul>
                <li className="">
                  <a href="/cv.pdf" target="__blank">
                    CV
                  </a>
                </li>
                <li className={activeClass('contacts', page)}>
                  <a onClick={() => navigateTo('contacts')}>Contacts</a>
                </li>
              </ul>
            </div>
          </nav>

          <div className="block block-social">
            <div className="block-title">Get in touch</div>
            <div className="block-content">
              <div className="social-icons">
                <a
                  rel="me"
                  href="mailto:max.hychka@gmail.com"
                  className="button button-icon u-email"
                  title="email: max.hychka@gmail.com"
                >
                  <i className="fa fa-envelope" />
                </a>
                <a
                  rel="me"
                  href="https://www.linkedin.com/in/mhychka"
                  className="button button-icon"
                  title="linkedin: mhychka"
                >
                  <i className="fa fa-linkedin-square" />
                </a>
                <a
                  rel="me"
                  href="https://facebook.com/mhychka"
                  className="button button-icon"
                  title="facebook: mhychka"
                >
                  <i className="fa fa-facebook-square" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </aside>
    )
  }

  render() {
    const { children } = this.props
    const { minHeight } = this.state

    return (
      <div>
        {this.renderLeftPanel()}
        <div className="site-main">
          <div className="site-main-inner">
            {React.cloneElement(children, {
              style: minHeight > 0 ? { minHeight: `${minHeight}px` } : {}
            })}
          </div>
        </div>
      </div>
    )
  }
}

export default AppLayout
