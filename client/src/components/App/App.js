import React, { Component } from 'react'

import { AppLayout } from '../AppLayout'
import { HomePageContainer } from 'pages/HomePage'
import { ProjectsPageContainer } from 'pages/ProjectsPage'
import { PhilosophyPageContainer } from 'pages/PhilosophyPage'
import { ContactsPageContainer } from 'pages/ContactsPage'
import { ExperiencePageContainer } from 'pages/ExperiencePage'
import { AboutPageContainer } from 'pages/AboutPage'
import { TechnologiesPageContainer } from 'pages/TechnologiesPage'
import { ConditionsPageContainer } from 'pages/ConditionsPage'
import { CodeExamplesPageContainer } from 'pages/CodeExamplesPage'
import { MusicPageContainer } from 'pages/MusicPage'

class App extends Component {
  componentWillMount() {
    const { initNavigation } = this.props
    initNavigation()
  }

  renderPage() {
    const { page } = this.props

    if (page === 'experience') {
      return <ExperiencePageContainer />
    }

    if (page === 'technologies') {
      return <TechnologiesPageContainer />
    }

    if (page === 'projects') {
      return <ProjectsPageContainer />
    }

    if (page === 'about') {
      return <AboutPageContainer />
    }

    if (page === 'philosophy') {
      return <PhilosophyPageContainer />
    }

    if (page === 'contacts') {
      return <ContactsPageContainer />
    }

    if (page === 'conditions') {
      return <ConditionsPageContainer />
    }

    if (page === 'code') {
      return <CodeExamplesPageContainer />
    }

    if (page === 'music') {
      return <MusicPageContainer />
    }

    return <HomePageContainer />
  }

  render() {
    return (
      <div>
        <AppLayout {...this.props}>{this.renderPage()}</AppLayout>
      </div>
    )
  }
}

export default App
