import React, { Component } from 'react'
import { guid } from 'utils/string'
import { loadWidgetScript } from 'utils/widget'

class Widget extends Component {
  state = { boxId: null }

  componentDidMount() {
    loadWidgetScript(() => this.init(this.props))
  }

  componentWillReceiveProps(nextProps) {
    const { params = {}, feedKey, contestKey, contestId, surveyKey } = nextProps

    if (
      Object.values(this.props.params || {}).join() !==
      Object.values(params).join()
    ) {
      this.update(params)
    }

    if (
      this.props.feedKey !== feedKey ||
      this.props.contestKey !== contestKey ||
      this.props.contestId !== contestId ||
      this.props.surveyKey !== surveyKey
    ) {
      this.destroy()
      this.init(nextProps)
    }
  }

  componentWillUnmount() {
    this.destroy()
  }

  update(params) {
    if (!window.ChoicelyWidget) {
      return
    }
    const widget = window.ChoicelyWidget.getInstance(this.state.boxId)
    if (widget) {
      widget.updateParams(params, true)
    }
  }

  init = props => {
    const {
      events = {},
      surveyKey = null,
      surveyAnswerKey,
      feedKey = null,
      widgetKey = null,
      articleKey,
      contestId,
      contestKey,
      params = {}
    } = props

    if (window.ChoicelyWidget) {
      this.setState({ boxId: `choicely-widget-${guid()}` })
      setTimeout(() => {
        if (window.ChoicelyWidget.getInstance(this.state.boxId)) {
          return false
        }
        const p = Object.assign(params, {
          niu: false,
          cid: contestId || contestKey,
          ak: articleKey,
          sk: surveyKey,
          sak: surveyAnswerKey,
          fk: feedKey,
          wk: widgetKey,
          bid: this.state.boxId,
          m: 'admin'
        })

        const e = Object.assign(events)

        window.ChoicelyWidget.i(p, e)
      }, 300)
    }
  }

  destroy() {
    if (window.ChoicelyWidget) {
      window.ChoicelyWidget.destroyAll()
    }
  }

  render() {
    const { isActive = true } = this.props

    return (
      <div
        id={this.state.boxId}
        style={{
          display: isActive ? 'block' : 'none'
        }}
      />
    )
  }
}

export default Widget
