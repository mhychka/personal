import React from 'react'
import universal from 'react-universal-component'
import { Empty } from 'components/Empty'
export default component => universal(component, { loading: <Empty /> })
