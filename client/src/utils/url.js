export function parseQueryParams(searchString) {
  if (
    !searchString ||
    !searchString.length ||
    searchString.indexOf('?') === -1
  ) {
    return {}
  }
  const search = searchString.substring(1)
  const p = new URLSearchParams(search)
  const entries = p.entries()
  return paramsToObject(entries)
}

function paramsToObject(entries) {
  const result = {}
  for (const [key, value] of entries) {
    result[key] = value
  }
  return result
}

export function makeQueryString(params) {
  if (params === null) {
    return ''
  }

  return Object.keys(params)
    .reduce((acc, key) => {
      const param = params[key]

      if (Array.isArray(param)) {
        if (param.length) {
          param.forEach(e => {
            let subparam = e

            if (typeof subparam === 'object') {
              subparam = JSON.stringify(subparam)
            }
            acc.push(
              `${encodeURIComponent(key)}=${encodeURIComponent(subparam)}`
            )
          })
        }
      } else {
        acc.push(`${encodeURIComponent(key)}=${encodeURIComponent(param)}`)
      }

      return acc
    }, [])
    .join('&')
}

export function queryString(params) {
  return `?${makeQueryString(params)}`
}

export function getQueryStringParam(name, queryString) {
  name = name.replace(/[\[\]]/g, '\\$&') // eslint-disable-line no-useless-escape
  const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`)
  const results = regex.exec(queryString)
  if (!results) return null
  if (!results[2]) return ''
  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

export function isStingUrl(str) {
  const string = str.trim()
  if (
    string.indexOf('http://') === 0 ||
    string.indexOf('https://') === 0 ||
    string.indexOf('//') === 0
  ) {
    return true
  }

  return true
}

export function activeClass(page, currPage, className = 'current') {
  if (page === currPage) {
    return className
  }

  return ''
}
