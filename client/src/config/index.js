const objectAssign = require('object-assign')
const base = require('./base')
const dev = require('./dev')
const prod = require('./prod')

let config

if (process.env.CONFIG === 'dev') {
  config = objectAssign({}, base, dev)
} else {
  config = objectAssign({}, base, prod)
}

module.exports = config
