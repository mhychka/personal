import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import 'idempotent-babel-polyfill'

import { AppContainer } from 'components/App'
import configureStore from './store'
const store = configureStore()

const element = (
  <Provider store={store}>
    <AppContainer />
  </Provider>
)

render(element, document.getElementById('app'))
