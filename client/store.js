import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import rootReducer from 'reducers/root'

export default function configureStore(initialState) {
  const store = compose(composeWithDevTools(applyMiddleware(thunkMiddleware)))(
    createStore
  )

  return store(rootReducer, initialState)
}
