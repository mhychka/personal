const koa = require('koa')
const serve = require('koa-static')
const koaRouter = require('koa-router')
const path = require('path')
const json = require('koa-json')
const cors = require('koa-cors')
const render = require('koa-swig')
const gzip = require('koa-gzip')

const config = require('../client/src/config')

const app = koa()
const router = koaRouter()
const distDir = path.resolve('./client/dist')
const assetsDir = path.resolve('./client/src/assets')
const templatesDir = path.resolve('./server/templates/')

app.use(serve(distDir))
app.use(serve(assetsDir))
app.use(json())
app.use(cors({ Origin: '*', 'Access-Control-Allow-Origin': '*' }))

app.context.render = render({
  root: templatesDir,
  autoescape: true,
  cache: 'memory',
  ext: 'html'
})

app
  .use(router.routes())
  .use(router.allowedMethods())
  .use(gzip())

app.listen(config.APP_PORT)
console.log(`Portfolio server started: ${config.APP_PORT}`)
