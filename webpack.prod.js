const webpack = require('webpack')
const path = require('path')
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin')
const replace = require('replace')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const config = require('./client/src/config')
const distPath = path.resolve('./client/dist')
const publicPath = `${config.HOST_URL}/dist/`

const webpackConfig = {
  devtool: 'cheap-module-source-map',
  entry: {
    app: './client/client'
  },
  module: {
    strictExportPresence: true,
    rules: [
      {
        test: /\.(html)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]'
          }
        }
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      { test: /\.(jpe?g|png|gif|svg)$/, use: 'file-loader' },
      {
        test: /\.css$/,
        exclude: /inline.css$/,
        use: [
          ExtractCssChunks.loader,
          {
            loader: 'css-loader'
          }
        ]
      },
      {
        test: /inline.css$/,
        loader: 'inline-postcss-webpack-loader'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['client/src', 'node_modules']
  },
  output: {
    path: distPath,
    filename: '[name].js',
    chunkFilename: '[name]-[hash].js',
    publicPath,
    pathinfo: true
  },
  watch: false,
  plugins: [
    //new BundleAnalyzerPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new ExtractCssChunks({
      filename: '[name]-[hash].css'
    }),
    new webpack.ExtendedAPIPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'process.env.BROWSER': JSON.stringify(true),
      __DEV__: false,
      __STRICT_MODE__: false,
      NODE_ENV: JSON.stringify(process.env.NODE_ENV)
    }),
    {
      apply: compiler => {
        compiler.hooks.afterEmit.tap('AfterEmitPlugin', compilation => {
          replace({
            regex: /cacheid-[0-9]*/,
            replacement:
              'cacheid-' +
              Math.random()
                .toString()
                .substr(2, 7),
            paths: ['client/src/assets'],
            recursive: true,
            silent: true,
            includes: '*.html'
          })
        })
      }
    }
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        default: false
      }
    }
  }
}

module.exports = webpackConfig
